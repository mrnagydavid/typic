module.exports = {
  testURL: "http://localhost/",
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  moduleFileExtensions: ["ts", "js", "json", "node"],
  collectCoverageFrom: ["<rootDir>/src/**/*.ts"],
  coveragePathIgnorePatterns: ["<rootDir>/src/index.ts"],
  testMatch: ["<rootDir>/tests/**/*.test.ts"],
};
