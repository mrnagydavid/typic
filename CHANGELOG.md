### 1.2.0

- Add `each` to JSON validation which validates every property of the JSON against a validator.
- Add `boolean` type to the validations.
