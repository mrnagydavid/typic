import { IStringValidator } from "../interfaces";
import { GeneralValidator } from "./general";

export class StringValidator implements IStringValidator {
  private _is = false;
  constructor(private value: string) {
    this._is = this.is();
  }

  public binary(): boolean {
    return this._is && this.regex(/^[01]+$/);
  }

  public creditcard(): boolean {
    return this._is && this.regex(/^\d{4}[\s\-]{0,1}\d{4}[\s\-]{0,1}\d{4}[\s\-]{0,1}\d{4}$/g) && this.luhn();
  }

  public decimal(): boolean {
    return this._is && this.regex(/^[0-9]+$/);
  }

  public equals(str: string): boolean {
    return this.value === str;
  }

  public hasLowercase(): boolean {
    // return this._is && this.regex(/\p{Ll}/);
    return this._is && this.value.toUpperCase() !== this.value;
  }

  public hasUppercase(): boolean {
    // return this._is && this.regex(/\p{Lu}/);
    return this._is && this.value.toLowerCase() !== this.value;
  }

  public hasNumber(): boolean {
    // return this._is && this.regex(/\p{Nd}/);
    return this._is && this.regex(/\d/);
  }

  public hasPunctuation(): boolean {
    // return this._is && this.regex(/\p{P}/);
    return this._is && this.regex(/[\.\,\?\!\;]/);
  }

  public hexa(): boolean {
    // return this._is && this.regex(/^(0|[1-9a-fA-F][0-9a-fA-F])*$/);
    return this._is && this.regex(/^[0-9a-fA-F]+$/);
  }

  public is(): boolean {
    return typeof this.value === "string";
  }

  public luhn(): boolean {
    return this._is && GeneralValidator.checkLuhn(this.value);
  }

  public minLength(len: number): boolean {
    return this._is && this.value.length >= len;
  }

  public maxLength(len: number): boolean {
    return this._is && this.value.length <= len;
  }

  public regex(pattern: RegExp): boolean {
    return this._is && pattern.test(this.value);
  }

  public regexp(pattern: RegExp): boolean {
    return this.regex(pattern);
  }
}
