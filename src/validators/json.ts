import { IValidator, IJSON } from "../interfaces";
import { ObjectValidator } from "./object";

export class JSONValidator extends ObjectValidator implements IValidator {
  constructor(value: IJSON) {
    let parsedValue = value;
    if (typeof value === "string") {
      try {
        parsedValue = JSON.parse(value);
      } catch (err) {
        //
      } finally {
        super(parsedValue);
      }
    } else {
      super(parsedValue);
    }
  }
}
