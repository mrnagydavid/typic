import { IArrayValidator } from "../interfaces";

export class ArrayValidator implements IArrayValidator {
  private _is = false;

  constructor(private value: any[]) {
    this._is = this.is();
  }

  public is(): boolean {
    return Array.isArray(this.value);
  }

  public minLength(limit: number): boolean {
    return this._is && this.value.length >= limit;
  }

  public maxLength(limit: number): boolean {
    return this._is && this.value.length <= limit;
  }

  public length(length: number): boolean {
    return this._is && this.value.length === length;
  }
}
