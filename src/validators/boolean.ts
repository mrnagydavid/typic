import { IBooleanValidator } from "../interfaces";

export class BooleanValidator implements IBooleanValidator {
  private _is = false;
  constructor(private value: boolean) {
    this._is = this.is();
  }

  public is(): boolean {
    return typeof this.value === "boolean";
  }

  public true(): boolean {
    return this._is && this.value === true;
  }

  public false(): boolean {
    return this._is && this.value === false;
  }
}
