import { IValidator } from "../interfaces";

export class ObjectValidator implements IValidator {
  constructor(private value: object) {}

  public is(): boolean {
    return typeof this.value === "object" && this.value !== null && typeof this.value.hasOwnProperty === "function";
  }
}
