import { INumberValidator } from "../interfaces";
import { GeneralValidator } from "./general";

export class NumberValidator implements INumberValidator {
  private _is = false;
  constructor(private value: number) {
    this._is = this.is();
  }

  public is(): boolean {
    return typeof this.value === "number" && !Number.isNaN(this.value) && Number.isFinite(this.value);
  }

  public close(num: number, epsilon: number): boolean {
    return this._is && Math.abs(this.value - num) < epsilon;
  }

  public equals(num: number): boolean {
    return this._is && this.value === num;
  }

  public integer(): boolean {
    return this._is && Number.isInteger(this.value);
  }

  public luhn(): boolean {
    return this._is && GeneralValidator.checkLuhn(this.value);
  }

  public max(ceil: number): boolean {
    return this._is && this.value <= ceil;
  }

  public min(floor: number): boolean {
    return this._is && this.value >= floor;
  }

  public multipleOf(num: number): boolean {
    return this._is && this.value % num === 0;
  }

  public nonnegative(): boolean {
    return this._is && this.value >= 0;
  }

  public prime(): boolean {
    if (!this._is) {
      return false;
    } else if (this.value <= 1) {
      return false;
    } else if (this.value <= 3) {
      return true;
    } else if (this.value % 2 === 0 || this.value % 3 === 0) {
      return false;
    }
    let i = 5;
    while (i ** 2 <= this.value) {
      if (this.value % i === 0 || this.value % (i + 2) === 0) {
        return false;
      }
      i += 6;
    }
    return true;
  }

  public positive(): boolean {
    return this._is && this.value > 0;
  }

  public range(bounds: [number | undefined, number | undefined]): boolean {
    const [min, max] = bounds;
    return (
      this._is &&
      (typeof min === "undefined" || this.value - min > 0) &&
      (typeof max === "undefined" || max - this.value > 0)
    );
  }

  public regex(pattern: RegExp): boolean {
    return this._is && GeneralValidator.regex(pattern, this.value);
  }

  public regexp(pattern: RegExp): boolean {
    return this._is && GeneralValidator.regex(pattern, this.value);
  }
}
