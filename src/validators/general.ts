export class GeneralValidator {
  public static checkLuhn(value: number | string) {
    let checkValue: string[] = [];
    if (typeof value === "number") {
      checkValue = value
        .toString()
        .split("")
        .reverse();
    } else if (typeof value === "string") {
      checkValue = value
        .replace(/\D+/g, "")
        .split("")
        .reverse();
    }
    if (checkValue.length === 0) {
      return false;
    }
    const sum = checkValue.reduce((acc, val, ind) => {
      const num = +val;
      return acc + (ind % 2 ? 2 * num - (num > 4 ? 9 : 0) : num);
    }, 0);
    return sum % 10 === 0;
  }

  public static regex(pattern: RegExp, value: string | number): boolean {
    const testValue = typeof value === "number" ? value.toString() : value;
    return pattern.test(testValue);
  }
}
