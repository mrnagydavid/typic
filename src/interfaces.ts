export interface IJSON {
  [key: string]: any;
}
export type ErrorItem = any;
export type ErrorList = ErrorItem[];
export interface IErrorDict {
  "": ErrorList;
}
export interface IErrorJSON {
  [key: string]: ErrorList | IErrorJSON | undefined;
}

export interface IResult {
  valid: boolean;
  errors: IJSON | undefined;
}

export interface ILogicResult extends IResult {
  valid: boolean;
  errors: (IErrorJSON & IErrorDict) | undefined;
  results: Array<IResult | ILogicResult>;
}

export interface ISimpleResult<T> extends IResult {
  valid: boolean;
  errors: IErrorDict | undefined;
  passed: T | undefined;
}

export interface IComplexResult<T> extends IResult {
  valid: boolean;
  errors: IErrorJSON | IErrorDict | undefined;
  passed: Partial<T> | undefined;
}

export interface IValidatorConstructor {
  new (value: any): IValidator;
}

export interface IStringValidatorConstructor {
  new (value: string): IStringValidator;
}

export interface INumberValidatorConstructor {
  new (value: number): INumberValidator;
}

export interface IBooleanValidatorConstructor {
  new (value: boolean): IBooleanValidator;
}

export interface IArrayValidatorConstructor {
  new (value: any[]): IArrayValidator;
}

export interface IValidator {
  is(): boolean;
}

export interface IStringValidator extends IValidator {
  binary(): boolean;
  creditcard(): boolean;
  decimal(): boolean;
  equals(str: string): boolean;
  hasLowercase(): boolean;
  hasUppercase(): boolean;
  hasNumber(): boolean;
  hasPunctuation(): boolean;
  hexa(): boolean;
  luhn(): boolean;
  minLength(len: number): boolean;
  maxLength(len: number): boolean;
  regex(pattern: RegExp): boolean;
  regexp(pattern: RegExp): boolean;
}

export interface INumberValidator extends IValidator {
  close(num: number, epsilon: number): boolean;
  equals(num: number): boolean;
  integer(): boolean;
  luhn(): boolean;
  max(ceil: number): boolean;
  min(floor: number): boolean;
  multipleOf(num: number): boolean;
  prime(): boolean;
  nonnegative(): boolean;
  positive(): boolean;
  range(bounds: [number | undefined, number | undefined]): boolean;
  regex(pattern: RegExp): boolean;
  regexp(pattern: RegExp): boolean;
}

export interface IBooleanValidator extends IValidator {
  true(): boolean;
  false(): boolean;
}

export interface IArrayValidator extends IValidator {
  minLength(limit: number): boolean;
  maxLength(limit: number): boolean;
  length(length: number): boolean;
}

export interface IValidateable {
  validate(value: any): IResult;
}
