import { IValidatorConstructor, ISimpleResult, ErrorList, ErrorItem } from "../interfaces";
import { Schema } from "./schema";

export abstract class SimpleSchema<P extends Schema, T> extends Schema {
  protected state: {
    valid: boolean;
    errors: ErrorList;
  };

  constructor(ValidatorClass: IValidatorConstructor, fail: string) {
    super(ValidatorClass, fail);
  }

  public abstract and(options: Array<(sch: P) => P>, fail: string): P;
  public abstract or(options: Array<(sch: P) => P>, fail: string): P;
  public abstract not(condition: (sch: P) => P, fail: string): P;

  public validate(value: T): ISimpleResult<T> {
    this.beforeValidate(value);
    if (this.state.valid === true) {
      this.validations.forEach(validation => {
        validation(value);
      });
    }
    if (!this.state.valid && this._optional) {
      return {
        valid: true,
        errors:
          this.state.errors.length > 0
            ? {
                "": this.state.errors,
              }
            : undefined,
        passed: undefined,
      };
    } else {
      return {
        valid: this.state.valid,
        errors:
          this.state.errors.length > 0
            ? {
                "": this.state.errors,
              }
            : undefined,
        passed: this.state.valid ? value : undefined,
      };
    }
  }

  protected addError(fail: string) {
    this.state.valid = false;
    this.state.errors.push(fail);
  }

  protected addValidation(validationFn: (value: T) => boolean, fail: string) {
    this.validations.push((value: T) => {
      if (!validationFn(value)) {
        this.addError(fail);
      }
    });
  }

  protected addOrValidation(options: P[], fail: ErrorItem = `None of the OR options were valid!`) {
    this.addValidation((value: T) => {
      for (const schema of options) {
        const result = schema.validate(value);
        if (result.valid) {
          return true;
        }
      }
      return false;
    }, fail);
  }

  protected addAndValidation(options: P[], fail: ErrorItem = `None of the AND options were valid!`) {
    this.addValidation((value: T) => {
      for (const schema of options) {
        const result = schema.validate(value);
        if (!result.valid) {
          return false;
        }
      }
      return true;
    }, fail);
  }

  protected addNotValidation(condition: P, fail: ErrorItem = `The NOT condition was valid!`) {
    this.addValidation((value: T) => {
      const result = condition.validate(value);
      return !result.valid;
    }, fail);
  }

  protected resetState() {
    this.state = {
      valid: true,
      errors: [],
    };
  }
}
