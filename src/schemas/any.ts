import { ILogicResult, IResult, IErrorJSON, IErrorDict, IValidateable, ErrorItem } from "../interfaces";
import { NumberSchema } from "./number";
import { NumberValidator } from "../validators/number";
import { StringSchema } from "./string";
import { StringValidator } from "../validators/string";
import { JSONSchema } from "./json";
import { JSONValidator } from "../validators/json";
import { ArraySchema } from "./array";
import { ArrayValidator } from "../validators/array";
import { BooleanSchema } from "./boolean";
import { BooleanValidator } from "../validators/boolean";

interface ILockedAnySchema {
  validate(value: any): ILogicResult;
}

export class AnySchema implements IValidateable {
  private logic: "OR" | "AND" | "NOT" | undefined;
  private fail: string;
  private schemas: IValidateable[] = [];

  /**
   * Validates the current schema chain.
   * @param value The value to be evaluated against the schema.
   */
  public validate(value: any): ILogicResult {
    const results: Array<IResult | ILogicResult> = [];
    let valid = this.setStartState();
    const errors: IErrorJSON & IErrorDict = {
      "": [],
    };
    let index = -1;
    for (const schema of this.schemas) {
      ++index;
      const result = schema.validate(value);
      results.push(result);
      switch (this.logic) {
        case "OR":
          valid = valid || result.valid;
          break;
        case "AND":
          valid = valid && result.valid;
          break;
        case "NOT":
          valid = !result.valid;
          break;
      }
      if (!valid) {
        errors[""] = [this.fail];
        if (this.logic !== "NOT") {
          errors[index] = result.errors;
        }
      }
      if ((valid && this.logic === "OR") || (!valid && this.logic === "AND")) {
        break;
      }
    }
    return {
      valid,
      errors,
      results,
    };
  }

  /**
   * Sets the current schema chain to *ARRAY* evaluation.
   * @param fail Error message to be returned if the value to validate is not an Array.
   */
  public array(fail?: ErrorItem): ArraySchema {
    return new ArraySchema(ArrayValidator, fail);
  }

  /**
   * Sets the current schema chain to *NUMBER* evaluation.
   * @param fail Error message to be returned if the value to validate is not a Number.
   */
  public number(fail?: ErrorItem): NumberSchema {
    return new NumberSchema(NumberValidator, fail);
  }

  /**
   * Sets the current schema chain to *STRING* evaluation.
   * @param fail Error message to be returned if the value to validate is not an String.
   */
  public string(fail?: ErrorItem): StringSchema {
    return new StringSchema(StringValidator, fail);
  }

  /**
   * Sets the current schema chain to *BOOLEAN* evaluation.
   * @param fail Error message to be returned if the value to validate is not a Boolean.
   */
  public boolean(fail?: ErrorItem): BooleanSchema {
    return new BooleanSchema(BooleanValidator, fail);
  }

  /**
   * Sets the current schema chain to *Object/JSON* evaluation.
   * @param fail Error message to be returned if the value to validate is not an Object/JSON.
   */
  public json(fail?: ErrorItem): JSONSchema {
    return new JSONSchema(JSONValidator, fail);
  }

  /**
   * Sets the current schema chain to *AND* evaluation. Calls to other evaluation logic functions (OR, NOT) afterwards will be ineffective.
   * @param options List of functions where each return a schema chain to be evaluated.
   * @returns The current schema instance locked to *AND* evaluation.
   */
  public and(options: IValidateable[], fail: ErrorItem = `Not all of the schemas validated!`): ILockedAnySchema {
    if (!this.logic) {
      this.logic = "AND";
      this.fail = fail;
      this.addOptions(options);
    }
    return this;
  }

  /**
   * Sets the current schema chain to *OR* evaluation. Calls to other evaluation logic functions (AND, NOT) afterwards will be ineffective.
   * @param options List of functions where each return a schema chain to be evaluated.
   * @returns The current schema instance locked to *OR* evaluation.
   */
  public or(options: IValidateable[], fail: ErrorItem = `None of the schemas validated!`): ILockedAnySchema {
    if (!this.logic) {
      this.logic = "OR";
      this.fail = fail;
      this.addOptions(options);
    }
    return this;
  }

  /**
   * Sets the current schema chain to *NOT* evaluation. Calls to other evaluation logic functions (AND, OR) afterwards will be ineffective.
   * @param option A function that returns a schema chain to be evaluated.
   * @returns The current schema instance locked to *NOT* evaluation.
   */
  public not(option: IValidateable, fail: ErrorItem = `The schema validated!`): ILockedAnySchema {
    if (!this.logic) {
      this.logic = "NOT";
      this.fail = fail;
      this.addOptions([option]);
    }
    return this;
  }

  private addOptions(options: IValidateable[]): void {
    this.schemas.push(...options);
  }

  private setStartState(): boolean {
    switch (this.logic) {
      case "OR":
        return false;
      case "AND":
        return true;
      case "NOT":
        return false;
      default:
        return false;
    }
  }
}
