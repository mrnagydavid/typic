import { ErrorItem, ISimpleResult, IBooleanValidator, IBooleanValidatorConstructor } from "../interfaces";
import { SimpleSchema } from "./simple_schema";

export class BooleanSchema extends SimpleSchema<BooleanSchema, boolean> {
  protected _canCast = false;
  protected validator: IBooleanValidator;

  constructor(
    protected BooleanValidatorClass: IBooleanValidatorConstructor,
    fail: ErrorItem = "Value is not a boolean!"
  ) {
    super(BooleanValidatorClass, fail);
  }

  public validate(value: boolean): ISimpleResult<boolean> {
    value = this._canCast ? !!value : value;
    return super.validate(value);
  }

  public cast(): BooleanSchema {
    this._canCast = true;
    return this;
  }

  public true(fail: ErrorItem = `Value should be true!`): BooleanSchema {
    this.addValidation(() => this.validator.true(), fail);
    return this;
  }

  public false(fail: ErrorItem = `Value should be false!`): BooleanSchema {
    this.addValidation(() => this.validator.false(), fail);
    return this;
  }

  public or(options: Array<(sch: BooleanSchema) => BooleanSchema>, fail?: ErrorItem): BooleanSchema {
    const schemas = options.map(option => option(new BooleanSchema(this.BooleanValidatorClass, this.fail)));
    this.addOrValidation(schemas, fail);
    return this;
  }

  public and(options: Array<(sch: BooleanSchema) => BooleanSchema>, fail?: ErrorItem): BooleanSchema {
    const schemas = options.map(option => option(new BooleanSchema(this.BooleanValidatorClass, this.fail)));
    this.addAndValidation(schemas, fail);
    return this;
  }

  public not(condition: (sch: BooleanSchema) => BooleanSchema, fail?: ErrorItem): BooleanSchema {
    this.addNotValidation(condition(new BooleanSchema(this.BooleanValidatorClass, this.fail)), fail);
    return this;
  }
}
