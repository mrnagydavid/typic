import { INumberValidatorConstructor, INumberValidator, ErrorItem, ISimpleResult } from "../interfaces";
import { SimpleSchema } from "./simple_schema";

export class NumberSchema extends SimpleSchema<NumberSchema, number> {
  protected _canCast = false;
  protected validator: INumberValidator;

  constructor(protected NumberValidatorClass: INumberValidatorConstructor, fail: ErrorItem = "Value is not a number!") {
    super(NumberValidatorClass, fail);
  }

  public validate(value: number): ISimpleResult<number> {
    value = this._canCast && typeof value === "string" ? Number.parseFloat(value) : value;
    return super.validate(value);
  }

  public cast(): NumberSchema {
    this._canCast = true;
    return this;
  }

  public close(
    num: number,
    epsilon: number,
    fail: ErrorItem = `Value should be within +/- ${epsilon} of ${num}!`
  ): NumberSchema {
    this.addValidation(() => this.validator.close(num, epsilon), fail);
    return this;
  }

  public equals(num: number, fail: ErrorItem = `Value should be ${num}!`): NumberSchema {
    this.addValidation(() => this.validator.equals(num), fail);
    return this;
  }

  public equalsOne(matches: number[], fail: ErrorItem = `Value should be one of ${matches.join(", ")!}`): NumberSchema {
    fail = `Value should be one of [${matches.join(", ")}]!`;
    this.addValidation(() => matches.some(match => this.validator.equals(match)), fail);
    return this;
  }

  public integer(fail: ErrorItem = `Value should be integer!`): NumberSchema {
    this.addValidation(() => this.validator.integer(), fail);
    return this;
  }

  public luhn(fail: ErrorItem = `Value should be a Luhn mod 10 number!`): NumberSchema {
    this.addValidation(() => this.validator.luhn(), fail);
    return this;
  }

  public max(ceil: number, fail: ErrorItem = `Value should be ${ceil} at most!`): NumberSchema {
    this.addValidation(() => this.validator.max(ceil), fail);
    return this;
  }

  public min(floor: number, fail: ErrorItem = `Value should be ${floor} at least!`): NumberSchema {
    this.addValidation(() => this.validator.min(floor), fail);
    return this;
  }

  public multipleOf(num: number, fail: ErrorItem = `Value should be multiple of ${num}!`): NumberSchema {
    this.addValidation(() => this.validator.multipleOf(num), fail);
    return this;
  }

  public nonnegative(fail: ErrorItem = `Value should be non-negative!`): NumberSchema {
    this.addValidation(() => this.validator.nonnegative(), fail);
    return this;
  }

  public prime(fail: ErrorItem = `Value should be a prime!`): NumberSchema {
    this.addValidation(() => this.validator.prime(), fail);
    return this;
  }

  public positive(fail: ErrorItem = `Value should be positive!`): NumberSchema {
    this.addValidation(() => this.validator.positive(), fail);
    return this;
  }

  public range(
    bound: [number | undefined, number | undefined],
    fail: ErrorItem = `Value should be a prime!`
  ): NumberSchema {
    this.addValidation(() => this.validator.range(bound), fail);
    return this;
  }

  public regex(pattern: RegExp, fail: ErrorItem = `Value should match the pattern!`): NumberSchema {
    this.addValidation(() => this.validator.regex(pattern), fail);
    return this;
  }

  public regexp(pattern: RegExp, fail: ErrorItem = `Value should match the pattern!`): NumberSchema {
    this.addValidation(() => this.validator.regex(pattern), fail);
    return this;
  }

  public or(options: Array<(sch: NumberSchema) => NumberSchema>, fail?: ErrorItem): NumberSchema {
    const schemas = options.map(option => option(new NumberSchema(this.NumberValidatorClass, this.fail)));
    this.addOrValidation(schemas, fail);
    return this;
  }

  public and(options: Array<(sch: NumberSchema) => NumberSchema>, fail?: ErrorItem): NumberSchema {
    const schemas = options.map(option => option(new NumberSchema(this.NumberValidatorClass, this.fail)));
    this.addAndValidation(schemas, fail);
    return this;
  }

  public not(condition: (sch: NumberSchema) => NumberSchema, fail?: ErrorItem): NumberSchema {
    this.addNotValidation(condition(new NumberSchema(this.NumberValidatorClass, this.fail)), fail);
    return this;
  }
}
