import { IComplexResult, IErrorJSON, ErrorItem } from "../interfaces";
import { Schema } from "./schema";

export abstract class ComplexSchema<T> extends Schema {
  protected state: {
    valid: boolean;
    errors: IErrorJSON;
  };

  public validate(value: T): IComplexResult<T> {
    let passed;
    this.beforeValidate(value);
    if (this.state.valid) {
      this.validations.forEach(validation => {
        validation(value);
      });
      passed = this.collectPassedElements(value);
    }
    if (!this.state.valid && this._optional) {
      return {
        valid: true,
        errors: Object.keys(this.state.errors).length > 0 ? this.state.errors : undefined,
        passed: undefined,
      };
    } else {
      return {
        valid: this.state.valid,
        errors: Object.keys(this.state.errors).length > 0 ? this.state.errors : undefined,
        passed,
      };
    }
  }

  protected addValidation(validationFn: (value: any[]) => boolean, key: string | number, fail: string) {
    this.validations.push((value: any) => {
      if (!validationFn(value)) {
        this.addError(fail, key);
      }
    });
  }

  protected addError(fail: ErrorItem | IErrorJSON, key: string | number = "", optional: boolean = false) {
    this.state.valid = optional;
    if (key === "") {
      if (!Array.isArray(this.state.errors[key])) {
        this.state.errors[key] = [];
      }
      (this.state.errors[key] as string[]).push(fail);
    } else {
      this.state.errors[key] = fail;
    }
  }

  protected abstract collectPassedElements(value?: T): T;
}
