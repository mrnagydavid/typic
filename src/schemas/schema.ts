import { IValidatorConstructor, IValidator, IResult, IValidateable, ErrorItem } from "../interfaces";

export abstract class Schema implements IValidateable {
  protected validations: any[] = [];
  protected validator: IValidator;
  protected _optional = false;

  constructor(protected ValidatorClass: IValidatorConstructor, protected fail: string) {}

  public optional(): this {
    this._optional = true;
    return this;
  }

  public abstract validate(value: any): IResult;

  protected abstract addError(fail: string): void;

  protected abstract resetState(): void;

  protected beforeValidate(value: any) {
    this.resetState();
    this.validator = new this.ValidatorClass(value);
    this.typeCheck(this.fail);
  }

  protected typeCheck(fail: ErrorItem): void {
    if (!this.validator.is()) {
      this.addError(fail);
    }
  }
}
