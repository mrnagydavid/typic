import { IStringValidator, IStringValidatorConstructor, ErrorItem, ISimpleResult } from "../interfaces";
import { SimpleSchema } from "./simple_schema";

export class StringSchema extends SimpleSchema<StringSchema, string> {
  protected _canCast = false;
  protected validator: IStringValidator;

  constructor(protected StringValidatorClass: IStringValidatorConstructor, fail: ErrorItem = "Value is not a string!") {
    super(StringValidatorClass, fail);
  }

  public validate(value: string): ISimpleResult<string> {
    if (this._canCast && typeof value !== "string" && typeof (value as any).toString === "function") {
      value = (value as any).toString();
    }
    return super.validate(value);
  }

  public cast(): StringSchema {
    this._canCast = true;
    return this;
  }

  public binary(fail: ErrorItem = `Value should be a binary sequence!'`): StringSchema {
    this.addValidation(() => this.validator.binary(), fail);
    return this;
  }

  public creditcard(fail: ErrorItem = `Value should be a credit card number sequence!`): StringSchema {
    this.addValidation(() => this.validator.creditcard(), fail);
    return this;
  }

  public decimal(fail: ErrorItem = `Value should be a decimal sequence!'`): StringSchema {
    this.addValidation(() => this.validator.decimal(), fail);
    return this;
  }

  public equals(match: string, fail?: ErrorItem): StringSchema {
    fail = fail || `Value should match "${match}"!`;
    this.addValidation(() => this.validator.equals(match), fail);
    return this;
  }

  public equalsOne(matches: string[], fail?: ErrorItem): StringSchema {
    fail = `Value should be one of [${matches.join(", ")}]!`;
    this.addValidation(() => matches.some(match => this.validator.equals(match)), fail);
    return this;
  }

  public hasLowercase(fail: ErrorItem = `Value should contain a lowercase character!'`): StringSchema {
    this.addValidation(() => this.validator.hasLowercase(), fail);
    return this;
  }

  public hasUppercase(fail: ErrorItem = `Value should contain an uppercase character!'`): StringSchema {
    this.addValidation(() => this.validator.hasUppercase(), fail);
    return this;
  }

  public hasNumber(fail: ErrorItem = `Value should contain a digit!'`): StringSchema {
    this.addValidation(() => this.validator.hasNumber(), fail);
    return this;
  }

  public hasPunctuation(fail: ErrorItem = `Value should contain punctuation!'`): StringSchema {
    this.addValidation(() => this.validator.hasPunctuation(), fail);
    return this;
  }

  public hexa(fail: ErrorItem = `Value should be a hexadecimal sequence!'`): StringSchema {
    this.addValidation(() => this.validator.hexa(), fail);
    return this;
  }

  public luhn(fail: ErrorItem = `Value should be a Luhn mod 10 number sequence!`): StringSchema {
    this.addValidation(() => this.validator.luhn(), fail);
    return this;
  }

  public minLength(len: number, fail: ErrorItem = `Value should be at least ${len} characters long!`): StringSchema {
    this.addValidation(() => this.validator.minLength(len), fail);
    return this;
  }

  public maxLength(len: number, fail: ErrorItem = `Value should be at most ${len} characters long!`): StringSchema {
    this.addValidation(() => this.validator.maxLength(len), fail);
    return this;
  }

  public or(options: Array<(sch: StringSchema) => StringSchema>, fail?: ErrorItem): StringSchema {
    const schemas = options.map(option => option(new StringSchema(this.StringValidatorClass, this.fail)));
    this.addOrValidation(schemas, fail);
    return this;
  }

  public and(options: Array<(sch: StringSchema) => StringSchema>, fail?: ErrorItem): StringSchema {
    const schemas = options.map(option => option(new StringSchema(this.StringValidatorClass, this.fail)));
    this.addAndValidation(schemas, fail);
    return this;
  }

  public not(condition: (sch: StringSchema) => StringSchema, fail?: ErrorItem): StringSchema {
    this.addNotValidation(condition(new StringSchema(this.StringValidatorClass, this.fail)), fail);
    return this;
  }

  public regex(pattern: RegExp, fail: ErrorItem = `Value should match the pattern!`): StringSchema {
    this.addValidation(() => this.validator.regex(pattern), fail);
    return this;
  }

  public regexp(pattern: RegExp, fail: ErrorItem = `Value should match the pattern!`): StringSchema {
    this.addValidation(() => this.validator.regexp(pattern), fail);
    return this;
  }
}
