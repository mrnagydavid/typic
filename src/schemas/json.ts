import { IValidatorConstructor, IValidator, IJSON, IValidateable, ErrorItem } from "../interfaces";
import { ComplexSchema } from "./complex_schema";

export class JSONSchema extends ComplexSchema<IJSON> {
  protected validator: IValidator;

  constructor(JSONValidatorClass: IValidatorConstructor, fail: ErrorItem = `Value is not a JSON!`) {
    super(JSONValidatorClass, fail);
  }

  public allowedKeys(
    keys: string[],
    fail: ErrorItem = `JSON can only contain these keys: ${keys.join(", ")!}`
  ): JSONSchema {
    const keySet: Set<string> = new Set(keys);
    this.addValidation(
      (value: IJSON) => {
        return Object.keys(value).reduce((acc, key) => {
          const valid = keySet.has(key);
          if (!valid) {
            this.addError(fail, key);
          }
          return acc && valid;
        }, true);
      },
      "",
      fail
    );
    return this;
  }

  public keys(keys: { [key: string]: IValidateable }, fail: ErrorItem = `JSON doesn't match key schema!`): JSONSchema {
    this.addValidation(
      (value: IJSON) => {
        return Object.keys(keys).reduce((acc, key) => {
          const { valid, errors } = keys[key].validate(value[key]);
          if (errors) {
            this.addError(errors, key, valid);
          }
          return acc && valid;
        }, true);
      },
      "",
      fail
    );
    return this;
  }

  public each(
    validation: IValidateable,
    fail: ErrorItem = `JSON keys should satisfy the 'each' condition!`
  ): JSONSchema {
    this.addValidation(
      (value: IJSON) => {
        return Object.keys(value).reduce((acc, key) => {
          const { valid, errors } = validation.validate(value[key]);
          if (errors) {
            this.addError(errors, key, valid);
          }
          return acc && valid;
        }, true);
      },
      "",
      fail
    );
    return this;
  }

  protected resetState() {
    this.state = {
      valid: true,
      errors: {},
    };
  }

  protected collectPassedElements(value: IJSON): IJSON {
    const keys: Set<string> = new Set();
    const passed: IJSON = {};
    Object.keys(this.state.errors).forEach(key => {
      if (key !== "") {
        keys.add(key);
      }
    });
    Object.keys(value).forEach(key => {
      if (!keys.has(key)) {
        passed[key] = value[key];
      }
    });
    return passed;
  }
}
