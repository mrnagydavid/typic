import { IArrayValidator, IArrayValidatorConstructor, IValidateable, IResult, IJSON, ErrorItem } from "../interfaces";
import { ComplexSchema } from "./complex_schema";

type ValidatorFn = (elem: any, index: number, arr: any[]) => IResult;

export class ArraySchema extends ComplexSchema<any[]> {
  protected state: {
    valid: boolean;
    errors: IJSON;
  };
  protected validator: IArrayValidator;

  constructor(ArrayValidatorClass: IArrayValidatorConstructor, fail: ErrorItem = `Value is not an array!`) {
    super(ArrayValidatorClass, fail);
  }

  public each(
    validation: IValidateable | ValidatorFn,
    fail: ErrorItem = `Array elements should satisfy the 'each' condition!`
  ): ArraySchema {
    let validator: (elem: any, index: number, value: any[]) => IResult;
    validator =
      typeof validation === "function"
        ? (elem: any, index: number, value: any[]) => validation(elem, index, value)
        : (validator = (elem: any) => validation.validate(elem));

    this.addValidation(
      (value: any[]) => {
        return value.reduce((acc, elem, index) => {
          const { valid, errors } = validator(elem, index, value);
          if (errors) {
            this.addError(errors, index, valid);
          }
          return acc && valid;
        }, true);
      },
      "",
      fail
    );
    return this;
  }

  public minLength(limit: number, fail: ErrorItem = `Array should have at least ${limit} elements!`): ArraySchema {
    this.addValidation(() => this.validator.minLength(limit), "", fail);
    return this;
  }

  public maxLength(limit: number, fail: ErrorItem = `Array should have at most ${limit} elements!`): ArraySchema {
    this.addValidation(() => this.validator.maxLength(limit), "", fail);
    return this;
  }

  public length(length: number, fail: ErrorItem = `Array should have exactly ${length} elements!`): ArraySchema {
    this.addValidation(() => this.validator.length(length), "", fail);
    return this;
  }

  public value(
    index: number,
    schema: IValidateable,
    fail: ErrorItem = `Array value [#${index}] didn't match the schema!`
  ): ArraySchema {
    this.addValidation(
      (value: any[]) => {
        const { valid, errors } = schema.validate(value[index]);
        if (errors) {
          this.addError(errors, index, valid);
        }
        return valid;
      },
      "",
      fail
    );
    return this;
  }

  public values(schemas: IValidateable[], fail: ErrorItem = `Array values didn't match the schema!`): ArraySchema {
    this.addValidation(
      (value: any[]) => {
        return schemas.reduce((acc, schema, index) => {
          const { valid, errors } = schema.validate(value[index]);
          if (errors) {
            this.addError(errors, index, valid);
          }
          return acc && valid;
        }, true);
      },
      "",
      fail
    );
    return this;
  }

  protected resetState() {
    this.state = {
      valid: true,
      errors: {},
    };
  }

  protected collectPassedElements(value: any[]): any[] {
    const indices: Set<number> = new Set();
    const passed: any[] = [];
    Object.keys(this.state.errors).forEach(key => {
      if (key !== "") {
        indices.add(+key);
      }
    });
    for (let i = 0; i < value.length; ++i) {
      passed[i] = indices.has(i) ? undefined : value[i];
    }
    return passed;
  }
}
