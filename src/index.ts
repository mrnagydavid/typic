import { AnySchema } from "./schemas/any";

function validatorFactory() {
  return new AnySchema();
}

export default validatorFactory;
