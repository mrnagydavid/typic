import { ArrayValidator } from "../../src/validators/array";
import { testLogger } from "../utils";

describe("ArrayValidator", () => {
  it("is", () => {
    const tests: Array<[any, boolean]> = [
      ["", false],
      [0, false],
      [{}, false],
      [true, false],
      [arguments, false],
      [[], true],
    ];
    testLogger(tests, test => {
      const validator = new ArrayValidator(test[0]);
      expect(validator.is()).toBe(test[1]);
    });
  });

  it("minLength", () => {
    const tests: Array<[any[], number, boolean]> = [[[], 0, true], [[], 1, false], [["a"], 1, true], [["a"], 2, false]];
    testLogger(tests, test => {
      const validator = new ArrayValidator(test[0]);
      expect(validator.minLength(test[1])).toBe(test[2]);
    });
  });

  it("maxLength", () => {
    const tests: Array<[any[], number, boolean]> = [
      [[], 0, true],
      [[], 1, true],
      [["a"], 1, true],
      [["a", "b"], 1, false],
    ];
    testLogger(tests, test => {
      const validator = new ArrayValidator(test[0]);
      expect(validator.maxLength(test[1])).toBe(test[2]);
    });
  });

  it("length", () => {
    const tests: Array<[any[], number, boolean]> = [
      [[], 0, true],
      [[], 1, false],
      [["a"], 1, true],
      [["a", "b"], 1, false],
    ];
    testLogger(tests, test => {
      const validator = new ArrayValidator(test[0]);
      expect(validator.length(test[1])).toBe(test[2]);
    });
  });
});
