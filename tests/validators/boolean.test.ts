import { testLogger } from "../utils";
import { BooleanValidator } from "../../src/validators/boolean";

describe("StringValidator", () => {
  it("is", () => {
    const tests: Array<[any, boolean]> = [
      [true, true],
      [false, true],
      [0, false],
      [{}, false],
      ["", false],
      [1, false],
      [{ foo: "bar" }, false],
      ["foo", false],
    ];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new BooleanValidator(test[0]);
      expect(validator.is()).toBe(test[1]);
    });
  });

  it("true", () => {
    const tests: Array<[any, boolean]> = [[true, true], [false, false], ["foo", false], [{ foo: "bar" }, false]];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new BooleanValidator(test[0]);
      expect(validator.true()).toBe(test[1]);
    });
  });

  it("false", () => {
    const tests: Array<[any, boolean]> = [[true, false], [false, true], ["foo", false], [{ foo: "bar" }, false]];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new BooleanValidator(test[0]);
      expect(validator.false()).toBe(test[1]);
    });
  });
});
