import { testLogger } from "../utils";
import { ObjectValidator } from "../../src/validators/object";

describe("ObjectValidator", () => {
  it("is", () => {
    const tests: Array<[any, boolean]> = [
      [{}, true],
      [{ a: "b" }, true],
      [{ a: "b" }, true],
      [[], true],
      [[0, 1], true],
      ["", false],
      ["a", false],
      [0, false],
      [true, false],
    ];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new ObjectValidator(test[0]);
      expect(validator.is()).toBe(test[1]);
    });
  });
});
