import { testLogger } from "../utils";
import { NumberValidator } from "../../src/validators/number";

describe("NumberValidator", () => {
  it("is", () => {
    const tests: Array<[any, boolean]> = [
      ["", false],
      ["a", false],
      ["0", false],
      ["0.1", false],
      [0, true],
      [0.1, true],
      [{}, false],
      [true, false],
      [Number.NaN, false],
    ];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new NumberValidator(test[0]);
      expect(validator.is()).toBe(test[1]);
    });
  });

  it("close", () => {
    const tests: Array<[number, number, number, boolean]> = [
      [1, 0, 1, false],
      [0.9, 0, 0.9, false],
      [0.109, 0.1, 0.01, true],
      [0.11, 0.1, 0.01, true],
      [11, 10, 2, true],
      [Math.PI, 3.14, 0.1, true],
    ];
    testLogger(tests, (test: [number, number, number, boolean]) => {
      const validator = new NumberValidator(test[0]);
      expect(validator.close(test[1], test[2])).toBe(test[3]);
    });
  });

  it("equals", () => {
    const tests: Array<[number, number, boolean]> = [[0, 0, true], [0.1, 0.1, true], [0.11 - 0.1, 0.1, false]];
    testLogger(tests, (test: [number, number, boolean]) => {
      const validator = new NumberValidator(test[0]);
      expect(validator.equals(test[1])).toBe(test[2]);
    });
  });

  it("integer", () => {
    const tests: Array<[number, boolean]> = [[0, true], [0.1, false]];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.integer()).toBe(test[1]);
    });
  });

  it("luhn", () => {
    const tests: Array<[number, boolean]> = [[79927398713, true], [79927398714, false], [4691050015091615, true]];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.luhn()).toBe(test[1]);
    });
  });

  it("max", () => {
    const tests: Array<[number, number, boolean]> = [
      [0, 1, true],
      [1, 1, true],
      [1, 0, false],
      [Number.MAX_VALUE, Number.POSITIVE_INFINITY, true],
      [Number.POSITIVE_INFINITY, Number.MAX_VALUE, false],
    ];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.max(test[1])).toBe(test[2]);
    });
  });

  it("min", () => {
    const tests: Array<[number, number, boolean]> = [[0, 1, false], [1, 1, true], [1, 0, true]];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.min(test[1])).toBe(test[2]);
    });
  });

  it("nonnegative", () => {
    const tests: Array<[number, boolean]> = [[-1, false], [0, true], [1, true]];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.nonnegative()).toBe(test[1]);
    });
  });

  it("prime", () => {
    const tests: Array<[any, boolean]> = [
      ["", false],
      [0, false],
      [1, false],
      [2, true],
      [3, true],
      [4, false],
      [5, true],
      [6, false],
      [7, true],
      [8, false],
      [9, false],
      [10, false],
      [11, true],
      [13, true],
      [121, false],
      [123, false],
      [127, true],
    ];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.prime()).toBe(test[1]);
    });
  });

  it("positive", () => {
    const tests: Array<[number, boolean]> = [[-1, false], [0, false], [1, true]];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.positive()).toBe(test[1]);
    });
  });

  it("range", () => {
    const tests: Array<[number, [number, number], boolean]> = [
      [0, [0, 1], false],
      [1, [0, 1], false],
      [0.5, [0, 1], true],
      [-1, [undefined, 1], true],
      [2, [undefined, 1], false],
      [0.5, [0, undefined], true],
      [-0.5, [0, undefined], false],
      [0, [undefined, undefined], true],
    ];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.range(test[1])).toBe(test[2]);
    });
  });

  it("regex/regexp", () => {
    const tests: Array<[number, RegExp, boolean]> = [
      [10, /^.*$/, true],
      [10, /^.+$/, true],
      [10, /^\w+$/, true],
      [10, /^\d+$/, true],
      [10, /^\D+$/, false],
    ];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.regex(test[1])).toBe(test[2]);
      expect(validator.regexp(test[1])).toBe(test[2]);
    });
  });

  it("multipleOf", () => {
    const tests: Array<[number, number, boolean]> = [
      [0, 1, true],
      [0, 2, true],
      [1, 1, true],
      [2, 1, true],
      [3, 2, false],
      [4, 2, true],
      [5, 2, false],
      [6, 2, true],
      [6, 3, true],
      [6, 4, false],
    ];
    testLogger(tests, test => {
      const validator = new NumberValidator(test[0]);
      expect(validator.multipleOf(test[1])).toBe(test[2]);
    });
  });
});
