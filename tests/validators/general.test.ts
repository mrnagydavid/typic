import { testLogger } from "../utils";
import { GeneralValidator } from "../../src/validators/general";

describe("GeneralValidator", () => {
  describe("checkLuhn", () => {
    it("should validate a Luhn sequence", () => {
      const tests: Array<[string | number, boolean]> = [
        ["", false],
        ["79927398713", true],
        ["79927398714", false],
        ["7992 7398 713", true],
        ["7992-7398-713", true],
        [4691050015091615, true],
        ["4691050015091615", true],
        ["4691 0500 1509 1615", true],
        ["4691-0500-1509-1615", true],
      ];
      testLogger(tests, (test: [string | number, boolean]) => {
        expect(GeneralValidator.checkLuhn(test[0])).toBe(test[1]);
      });
    });
  });
});
