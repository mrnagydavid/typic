import { testLogger } from "../utils";
import { JSONValidator } from "../../src/validators/json";

describe("JSONValidator", () => {
  it("accepts string if it is stringified JSON", () => {
    const tests: Array<[any, boolean]> = [
      ["{}", true],
      ['{ "a": "b" }', true],
      ['{ a: "b" }', false],
      ["[]", true],
      ["[0, 1]", true],
      ['""', false],
      ['"a"', false],
      ["0", false],
      ["true", false],
    ];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new JSONValidator(test[0]);
      expect(validator.is()).toBe(test[1]);
    });
  });
});
