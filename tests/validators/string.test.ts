import { testLogger } from "../utils";
import { StringValidator } from "../../src/validators/string";

describe("StringValidator", () => {
  it("is", () => {
    const tests: Array<[any, boolean]> = [["", true], ["a", true], [0, false], [{}, false], [true, false]];
    testLogger(tests, (test: [any, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.is()).toBe(test[1]);
    });
  });

  it("minLength", () => {
    const tests: Array<[string, number, boolean]> = [
      ["", 0, true],
      ["", 1, false],
      ["a", 0, true],
      ["a", 1, true],
      ["a", 2, false],
    ];
    testLogger(tests, (test: [string, number, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.minLength(test[1])).toBe(test[2]);
    });
  });

  it("maxLength", () => {
    const tests: Array<[string, number, boolean]> = [
      ["", 0, true],
      ["", 1, true],
      ["a", 0, false],
      ["a", 1, true],
      ["a", 2, true],
    ];
    testLogger(tests, (test: [string, number, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.maxLength(test[1])).toBe(test[2]);
    });
  });

  it("regex", () => {
    const tests: Array<[string, RegExp, boolean]> = [
      ["", /.*/, true],
      ["a", /.*/, true],
      ["a", /\w/, true],
      ["a", /\d/, false],
      ["1", /\d/, true],
    ];
    testLogger(tests, (test: [string, RegExp, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.regex(test[1])).toBe(test[2]);
      expect(validator.regexp(test[1])).toBe(test[2]);
    });
  });

  it("equals", () => {
    const tests: Array<[string, string, boolean]> = [
      ["", "", true],
      ["", "a", false],
      ["a", "", false],
      ["a", "a", true],
      ["a", "A", false],
    ];
    testLogger(tests, (test: [string, string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.equals(test[1])).toBe(test[2]);
    });
  });

  it("luhn", () => {
    const tests: Array<[string, boolean]> = [
      ["79927398713", true],
      ["79927398714", false],
      ["7992 7398 713", true],
      ["7992-7398-713", true],
      ["4691050015091615", true],
      ["4691 0500 1509 1615", true],
      ["4691-0500-1509-1615", true],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.luhn()).toBe(test[1]);
    });
  });

  it("creditcard", () => {
    const tests: Array<[string, boolean]> = [
      ["79927398713", false],
      ["79927398714", false],
      ["7992 7398 713", false],
      ["7992-7398-713", false],
      ["4691050015091615", true],
      ["4691 0500 1509 1615", true],
      ["4691-0500-1509-1615", true],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.creditcard()).toBe(test[1]);
    });
  });

  it("binary", () => {
    const tests: Array<[string, boolean]> = [
      ["", false],
      ["0", true],
      ["1", true],
      ["2", false],
      ["1010", true],
      ["101o", false],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.binary()).toBe(test[1]);
    });
  });

  it("hasLowercase", () => {
    const tests: Array<[string, boolean]> = [
      ["", false],
      ["0", false],
      ["a", true],
      ["A", false],
      ["AaAa", true],
      ["AAAA", false],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.hasLowercase()).toBe(test[1]);
    });
  });

  it("hasNumber", () => {
    const tests: Array<[string, boolean]> = [
      ["", false],
      ["0", true],
      ["a", false],
      ["A", false],
      ["a0a", true],
      ["AAAA", false],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.hasNumber()).toBe(test[1]);
    });
  });

  it("hasPunctuation", () => {
    const tests: Array<[string, boolean]> = [
      ["", false],
      ["0", false],
      ["a", false],
      ["A", false],
      ["a0a", false],
      ["AAAA", false],
      ["A,a", true],
      ["A;a", true],
      ["A.a", true],
      ["A?a", true],
      ["A!a", true],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.hasPunctuation()).toBe(test[1]);
    });
  });

  it("hexa", () => {
    const tests: Array<[string, boolean]> = [["", false], ["0", true], ["0123456789abcdef", true], ["defh", false]];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.hexa()).toBe(test[1]);
    });
  });

  it("decimal", () => {
    const tests: Array<[string, boolean]> = [
      ["", false],
      ["0", true],
      ["0123456789", true],
      ["0123456789abcdef", false],
    ];
    testLogger(tests, (test: [string, boolean]) => {
      const validator = new StringValidator(test[0]);
      expect(validator.decimal()).toBe(test[1]);
    });
  });

  it("should not run if the type is not correct", () => {
    const validator = new StringValidator(0 as any);
    expect(validator.minLength(1)).toBe(false);
    expect(validator.maxLength(1)).toBe(false);
  });
});
