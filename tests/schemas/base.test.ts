import { StringSchema } from "../../src/schemas/string";
import { StringValidator } from "../../src/validators/string";
import { testLogger } from "../utils";
import { JSONSchema } from "../../src/schemas/json";
import { JSONValidator } from "../../src/validators/json";

describe("Schema", () => {
  describe("optional", () => {
    it("validates even if the validation fails <SimpleSchema>", () => {
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.optional().equals("a");
      const tests: Array<[any, boolean]> = [
        ["", false],
        ["a", true],
        [0, false],
        [undefined, false],
        [null, false],
        [{}, false],
        [false, false],
      ];
      testLogger(tests, test => {
        const { valid, passed } = stringSchema.validate(test[0]);
        expect(valid).toBe(true);
        expect(passed).toEqual(test[1] ? test[0] : undefined);
      });
    });

    it("validates even if the validation fails <ComplexSchema>", () => {
      const jsonSchema = new JSONSchema(JSONValidator);
      jsonSchema.optional().allowedKeys(["a"]);
      const tests: Array<[any, boolean]> = [
        [{ a: 0 }, true],
        [{ b: 0 }, false],
        [{ a: 0, b: 0 }, false],
        ["", false],
        [0, false],
        [undefined, false],
        [null, false],
        [false, false],
      ];
      testLogger(tests, test => {
        const { valid, passed } = jsonSchema.validate(test[0]);
        expect(valid).toBe(true);
        expect(passed).toEqual(test[1] ? test[0] : undefined);
      });
    });
  });
});
