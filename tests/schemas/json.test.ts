import { testLogger, numSchema, strSchema, jsonSchema as jsonSchemaFactory } from "../utils";
import { JSONSchema } from "../../src/schemas/json";
import { JSONValidator } from "../../src/validators/json";
import { IJSON, IValidateable } from "../../src/interfaces";

describe("JSONSchema", () => {
  describe("keys", () => {
    it("should validate even the nested keys", () => {
      const a = 1;
      const aValidSchema = numSchema().positive();
      const aInvalidSchema = numSchema().min(2);

      const foo = "bar";
      const fooValidSchema = strSchema()
        .minLength(2)
        .maxLength(4);
      const fooInvalidSchema = strSchema().minLength(4);

      const dub = 1;
      const dubValidSchema = numSchema()
        .min(0)
        .max(2);
      const dubInvalidSchema = numSchema()
        .min(2)
        .prime();

      const sub = {
        dub,
      };
      const subValidSchema = jsonSchemaFactory().keys({
        dub: dubValidSchema,
      });
      const subInvalidSchema = jsonSchemaFactory().keys({
        dub: dubInvalidSchema,
      });

      const testObj = {
        a,
        foo,
        sub: {
          dub,
        },
      };
      const testValidSchema = {
        a: aValidSchema,
        foo: fooValidSchema,
        sub: subValidSchema,
      };
      const testInvalidSchema = {
        a: aInvalidSchema,
        foo: fooInvalidSchema,
        sub: subInvalidSchema,
      };
      const testInvalidSchemaErrors = {
        a: aInvalidSchema.validate(testObj.a).errors,
        foo: fooInvalidSchema.validate(testObj.foo).errors,
        sub: subInvalidSchema.validate(testObj.sub).errors,
      };
      const tests: Array<[any, any, boolean, any, any]> = [
        [testObj, testValidSchema, true, testObj, undefined],
        [testObj, testInvalidSchema, false, undefined, testInvalidSchemaErrors],
      ];
      testLogger(tests, test => {
        const jsonSchema = new JSONSchema(JSONValidator);
        jsonSchema.keys(test[1]);
        const { valid, errors, passed } = jsonSchema.validate(test[0]);
        expect(valid).toBe(test[2]);
        if (!valid) {
          expect(errors).toEqual(expect.objectContaining(test[4]));
        } else {
          expect(passed).toEqual(test[3]);
        }
      });
    });
  });

  describe("allowedKeys", () => {
    it("should validate if there are no other keys in the object", () => {
      const tests: Array<[IJSON, string[], boolean]> = [
        [{}, ["foo"], true],
        [{ foo: "bar" }, ["foo"], true],
        [{ bar: "foo" }, ["foo"], false],
        [{ foo: "bar", bar: "foo" }, ["foo"], false],
      ];
      testLogger(tests, test => {
        const jsonSchema = new JSONSchema(JSONValidator);
        jsonSchema.allowedKeys(test[1]);
        const { valid } = jsonSchema.validate(test[0]);
        expect(valid).toBe(test[2]);
      });
    });
  });

  describe("each", () => {
    it("should validate every key with the validator", () => {
      const tests: Array<[IJSON, IValidateable, boolean]> = [
        [{ foo: "bar" }, strSchema().minLength(3), true],
        [{ foo: "bar", bar: "foo" }, strSchema().minLength(3), true],
        [{ foo: "bar", bar: "f" }, strSchema().minLength(3), false],
        [{ foo: "bar", bar: 3 }, strSchema().minLength(3), false],
        [{ foo: "bar", bar: 3 }, numSchema().min(3), false],
        [{ foo: 4, bar: 3 }, numSchema().min(3), true],
      ];
      testLogger(tests, test => {
        const jsonSchema = new JSONSchema(JSONValidator);
        jsonSchema.each(test[1]);
        const { valid } = jsonSchema.validate(test[0]);
        expect(valid).toBe(test[2]);
      });
    });
  });
});
