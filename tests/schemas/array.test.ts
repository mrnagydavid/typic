import { testLogger, numSchema, strSchema, arrSchema, jsonSchema } from "../utils";
import { ArrayValidator } from "../../src/validators/array";
import { ArraySchema } from "../../src/schemas/array";
import { IJSON, IValidateable } from "../../src/interfaces";

describe("ArraySchema", () => {
  describe("each", () => {
    it("should return true if every element validates", () => {
      const schema = strSchema().minLength(2);
      const tests: Array<[any[], boolean]> = [
        [[0, 0], false],
        [[0, ""], false],
        [[0, "aa"], false],
        [["aa", "bb"], true],
        [["aa", "bb", 0], false],
      ];
      const arraySchema = new ArraySchema(ArrayValidator);
      arraySchema.each(schema);
      testLogger(tests, test => {
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });

    it("can use a function for validation", () => {
      const valFn = (elem: any, index: number) => {
        const schemas: IValidateable[] = [strSchema(), numSchema()];
        return schemas[index % 2].validate(elem);
      };
      const tests: Array<[any[], boolean]> = [
        [[0, 0], false],
        [[0, ""], false],
        [["", ""], false],
        [["", 0], true],
        [["", 0, 0], false],
        [["", 0, ""], true],
      ];
      const arraySchema = new ArraySchema(ArrayValidator);
      arraySchema.each(valFn);
      testLogger(tests, test => {
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });
  });

  describe("minLength", () => {
    it("should return true if the array length is at least the limit", () => {
      const tests: Array<[any[], number, boolean]> = [
        [[], 0, true],
        [[], 1, false],
        [["a"], 1, true],
        [["a"], 2, false],
      ];
      testLogger(tests, test => {
        const arraySchema = new ArraySchema(ArrayValidator);
        arraySchema.minLength(test[1]);
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[2]);
      });
    });
  });

  describe("maxLength", () => {
    it("should return true if the array length is at least the limit", () => {
      const tests: Array<[any[], number, boolean]> = [
        [[], 0, true],
        [[], 1, true],
        [["a"], 1, true],
        [["a", "b"], 1, false],
      ];
      testLogger(tests, test => {
        const arraySchema = new ArraySchema(ArrayValidator);
        arraySchema.maxLength(test[1]);
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[2]);
      });
    });
  });

  describe("length", () => {
    it("should return true if the array length is exactly the limit", () => {
      const tests: Array<[any[], number, boolean]> = [
        [[], 0, true],
        [[], 1, false],
        [["a"], 1, true],
        [["a", "b"], 1, false],
      ];
      testLogger(tests, test => {
        const arraySchema = new ArraySchema(ArrayValidator);
        arraySchema.length(test[1]);
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[2]);
      });
    });
  });

  describe("value", () => {
    it("should validate even the nested value", () => {
      const num = 0;
      const numValidSchema = numSchema().equals(0);
      const numInvalidSchema = numSchema().equals(1);

      const testObj: [any, number, any] = [{}, num, []];

      const tests: Array<[[any, number, any], any, boolean, IJSON]> = [
        [testObj, numValidSchema, true, undefined],
        [testObj, numInvalidSchema, false, { "1": numInvalidSchema.validate(testObj[1]).errors }],
      ];
      testLogger(tests, test => {
        const arraySchema = new ArraySchema(ArrayValidator);
        arraySchema.value(1, test[1]);
        const { valid, errors, passed } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[2]);
        if (valid) {
          expect(passed).toEqual(test[0]);
        } else {
          expect(errors).toEqual(expect.objectContaining(test[3]));
        }
      });
    });
  });

  describe("values", () => {
    it("should validate even the nested values", () => {
      const num = 0;
      const numValidSchema = numSchema().equals(0);
      const numInvalidSchema = numSchema().equals(1);

      const str = "a";
      const strValidSchema = strSchema().equals("a");
      const strInvalidSchema = strSchema().minLength(2);

      const arr = [0, "a"];
      const arrValidSchema = arrSchema().minLength(1);
      const arrInvalidSchema = arrSchema().maxLength(1);

      const json = { foo: "bar" };
      const jsonValidSchema = jsonSchema().keys({
        foo: strSchema().equals("bar"),
      });
      const jsonInvalidSchema = jsonSchema().keys({
        foo: strSchema().equals("foo"),
      });

      const testObj: [number, string, any[], IJSON] = [num, str, arr, json];
      const testValidSchema = [numValidSchema, strValidSchema, arrValidSchema, jsonValidSchema];
      const testInvalidSchema = [numInvalidSchema, strInvalidSchema, arrInvalidSchema, jsonInvalidSchema];
      const testInvalidSchemaErrors = {
        "0": numInvalidSchema.validate(testObj[0]).errors,
        "1": strInvalidSchema.validate(testObj[1]).errors,
        "2": arrInvalidSchema.validate(testObj[2]).errors,
        "3": jsonInvalidSchema.validate(testObj[3]).errors,
      };

      const tests: Array<[any[], any, boolean, any[], any]> = [
        [testObj, testValidSchema, true, testObj, undefined],
        [testObj, testInvalidSchema, false, undefined, testInvalidSchemaErrors],
      ];
      testLogger(tests, test => {
        const arraySchema = new ArraySchema(ArrayValidator);
        arraySchema.values(test[1]);
        const { valid, errors, passed } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[2]);
        if (valid) {
          expect(passed).toEqual(test[3]);
        } else {
          expect(errors).toEqual(expect.objectContaining(test[4]));
        }
      });
    });
  });
});
