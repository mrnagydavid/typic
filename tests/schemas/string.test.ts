import { StringSchema } from "../../src/schemas/string";
import { StringValidator } from "../../src/validators/string";
import { testLogger, strSchema } from "../utils";
import { IErrorDict } from "../../src/interfaces";

function checkErrorMessage(errors: IErrorDict, message?: string) {
  expect(errors).toBeTruthy();
  expect(errors).toHaveProperty("");
  expect(errors[""]).toBeInstanceOf(Array);
  expect(errors[""].length).toBe(1);
  if (typeof message !== "undefined") {
    expect(errors[""]).toEqual([message]);
  }
}

describe("StringSchema", () => {
  it("should be chainable", () => {
    const tests: Array<[keyof StringSchema, any]> = [["minLength", 1], ["maxLength", 1]];
    const stringSchema = new StringSchema(StringValidator);
    testLogger(tests, (test: any) => {
      expect(stringSchema[test[0]](test[1])).toBe(stringSchema);
    });
  });

  it("should return a default error message", () => {
    const stringSchema = new StringSchema(StringValidator);
    stringSchema.equals("a");
    const { valid, errors } = stringSchema.validate("b");
    checkErrorMessage(errors);
  });

  it("should return a custom error message", () => {
    const stringSchema = new StringSchema(StringValidator);
    stringSchema.equals("a", "NONONO");
    const { valid, errors } = stringSchema.validate("b");
    checkErrorMessage(errors, "NONONO");
  });

  describe("cast", () => {
    it("should call toString if asked for and available", () => {
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.cast();
      const tests: Array<[any, boolean]> = [
        ["", true],
        ["a", true],
        ["0", true],
        ["0.1", true],
        [0, true],
        [0.1, true],
        [{}, true],
        [true, true],
        [false, true],
        [Object.create(null), false],
        [Number.NaN, true],
      ];
      testLogger(tests, test => expect(stringSchema.validate(test[0]).valid).toBe(test[1]));
    });

    it("should give access to the cast value", () => {
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.cast();
      const tests: Array<[any, string]> = [
        ["", ""],
        ["a", "a"],
        ["0", "0"],
        ["0.1", "0.1"],
        [0, "0"],
        [0.1, "0.1"],
        [{}, {}.toString()],
        [true, "true"],
        [false, "false"],
        [Number.NaN, Number.NaN.toString()],
      ];
      testLogger(tests, test => expect(stringSchema.validate(test[0]).passed).toBe(test[1]));
    });

    it("should not call toString if not asked for", () => {
      const stringSchema = new StringSchema(StringValidator);
      const tests: Array<[any, boolean]> = [
        ["", true],
        ["a", true],
        ["0", true],
        ["0.1", true],
        [0, false],
        [0.1, false],
        [{}, false],
        [true, false],
        [false, false],
        [Object.create(null), false],
        [Number.NaN, false],
      ];
      testLogger(tests, test => expect(stringSchema.validate(test[0]).valid).toBe(test[1]));
    });
  });

  describe("equals", () => {
    it("should accept string value", () => {
      const tests: Array<[string, boolean]> = [["a", true], ["b", false]];
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.equals("a");
      testLogger(tests, (test: [string, boolean]) => {
        expect(stringSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });

    it("should do === checks", () => {
      const tests: Array<[string, any, boolean]> = [
        ["", "", true],
        ["", {}, false],
        ["123", 123, false],
        ["123", Number(123).toString(), true],
        ["", true, false],
      ];
      testLogger(tests, (test: [string, any, boolean]) => {
        const stringSchema = new StringSchema(StringValidator);
        stringSchema.equals(test[0]);
        expect(stringSchema.validate(test[1]).valid).toBe(test[2]);
      });
    });
  });

  describe("equalsOne", () => {
    it("should accept array value", () => {
      const tests: Array<[string, boolean]> = [["a", true], ["b", false], ["c", true]];
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.equalsOne(["c", "a"]);
      testLogger(tests, (test: [string, boolean]) => {
        expect(stringSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("minLength", () => {
    it("should be true if the string is longer than...", () => {
      const tests: Array<[string, number, boolean]> = [
        ["", 0, true],
        ["", 1, false],
        ["a", 0, true],
        ["a", 1, true],
        ["a", 2, false],
      ];
      testLogger(tests, (test: [string, number, boolean]) => {
        const stringSchema = new StringSchema(StringValidator);
        stringSchema.minLength(test[1]);
        expect(stringSchema.validate(test[0]).valid).toBe(test[2]);
      });
    });

    // it("should throw an Error if the parameter is incorrect", () => {
    //   const tests: Array<[any, ErrorConstructor]> = [
    //     ["", Error],
    //     ["1", Error],
    //     [-1, Error]
    //   ];
    //   testLogger(tests, (test: [any, ErrorConstructor]) => {
    //     const stringSchema = new StringSchema(StringValidator);
    //     expect(() => stringSchema.minLength(test[0])).toThrowError(test[1]);
    //   });
    // });
  });

  describe("maxLength", () => {
    it("should be true if the string is shorter than...", () => {
      const tests: Array<[string, number, boolean]> = [
        ["", 0, true],
        ["", 1, true],
        ["a", 0, false],
        ["a", 1, true],
        ["a", 2, true],
      ];
      testLogger(tests, (test: [string, number, boolean]) => {
        const stringSchema = new StringSchema(StringValidator);
        stringSchema.maxLength(test[1]);
        expect(stringSchema.validate(test[0]).valid).toBe(test[2]);
      });
    });

    // it("should throw an Error if the parameter is incorrect", () => {
    //   const tests: Array<[any, ErrorConstructor]> = [
    //     ["", Error],
    //     ["1", Error],
    //     [-1, Error]
    //   ];
    //   testLogger(tests, (test: [any, ErrorConstructor]) => {
    //     const stringSchema = new StringSchema(StringValidator);
    //     expect(() => stringSchema.maxLength(test[0])).toThrowError(test[1]);
    //   });
    // });
  });

  describe("creditcard", () => {
    it("should be true if the string is a creditcard number sequence", () => {
      const tests: Array<[string, boolean]> = [
        ["79927398713", false],
        ["79927398714", false],
        ["7992 7398 713", false],
        ["7992-7398-713", false],
        ["4691050015091615", true],
        ["4691 0500 1509 1615", true],
        ["4691-0500-1509-1615", true],
      ];
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.creditcard();
      testLogger(tests, (test: any) => {
        expect(stringSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("luhn", () => {
    it("should be true if the string is a creditcard number sequence", () => {
      const tests: Array<[string, boolean]> = [
        ["79927398713", true],
        ["79927398714", false],
        ["7992 7398 713", true],
        ["7992-7398-713", true],
        ["4691050015091615", true],
        ["4691 0500 1509 1615", true],
        ["4691-0500-1509-1615", true],
      ];
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.luhn();
      testLogger(tests, (test: any) => {
        expect(stringSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("regex/regexp", () => {
    it("should be true if the string matches the regex pattern", () => {
      const tests: Array<[string, RegExp, boolean]> = [
        ["", /.*/, true],
        ["a", /.*/, true],
        ["a", /\w/, true],
        ["a", /\d/, false],
        ["1", /\d/, true],
      ];
      testLogger(tests, (test: [string, RegExp, boolean]) => {
        const stringSchema1 = new StringSchema(StringValidator);
        stringSchema1.regex(test[1]);
        const stringSchema2 = new StringSchema(StringValidator);
        stringSchema2.regexp(test[1]);
        expect(stringSchema1.validate(test[0]).valid).toBe(test[2]);
        expect(stringSchema2.validate(test[0]).valid).toBe(test[2]);
      });
    });

    // it("should throw an Error if the parameter is incorrect", () => {
    //   const tests: Array<[any, ErrorConstructor]> = [
    //     ["/.*/", Error],
    //     [".*", Error],
    //     [0, Error],
    //   ];
    //   testLogger(tests, (test: [any, ErrorConstructor]) => {
    //     const stringSchema = new StringSchema(StringValidator);
    //     expect(() => stringSchema.minLength(test[0])).toThrowError(test[1]);
    //   });
    // });
  });

  describe("binary/decimal/hexa", () => {
    it("should be true if the string matches the numeric pattern", () => {
      const numPatt: Array<keyof StringValidator> = ["binary", "decimal", "hexa"];
      const tests: Array<[string, [boolean, boolean, boolean]]> = [
        ["", [false, false, false]],
        ["0", [true, true, true]],
        ["1", [true, true, true]],
        ["2", [false, true, true]],
        ["9", [false, true, true]],
        ["10", [true, true, true]],
        ["a", [false, false, true]],
        ["f", [false, false, true]],
        ["g", [false, false, false]],
      ];
      testLogger(tests, test => {
        numPatt.forEach((key, index) => {
          const stringSchema = new StringSchema(StringValidator);
          stringSchema[key]();
          expect(stringSchema.validate(test[0]).valid).toBe(test[1][index]);
        });
      });
    });
  });

  describe("hasLowercase/hasUppercase/hasNumber/hasPunctuation", () => {
    it("should be true if the string matches the numeric pattern", () => {
      const numPatt: Array<keyof StringValidator> = ["hasLowercase", "hasUppercase", "hasNumber", "hasPunctuation"];
      const tests: Array<[string, [boolean, boolean, boolean, boolean]]> = [
        ["", [false, false, false, false]],
        ["ő", [true, false, false, false]],
        ["Ő", [false, true, false, false]],
        ["0", [false, false, true, false]],
        [".", [false, false, false, true]],
        [",", [false, false, false, true]],
        [";", [false, false, false, true]],
        ["?", [false, false, false, true]],
        ["!", [false, false, false, true]],
        ["-", [false, false, false, false]],
        ["5!", [false, false, true, true]],
        ["Ő!", [false, true, false, true]],
        ["ő!", [true, false, false, true]],
        ["Í1!", [false, true, true, true]],
        ["í1!", [true, false, true, true]],
        ["Apple1.", [true, true, true, true]],
      ];
      testLogger(tests, test => {
        numPatt.forEach((key, index) => {
          const stringSchema = new StringSchema(StringValidator);
          stringSchema[key]();
          expect(stringSchema.validate(test[0]).valid).toBe(test[1][index]);
        });
      });
    });
  });

  describe("or", () => {
    it("should be true if some condition is true", () => {
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.or([sch => sch.equals("a"), sch => sch.equals("c")], "NONONO");
      const tests: Array<[string, boolean]> = [["a", true], ["b", false], ["c", true]];
      testLogger(tests, (test: [string, boolean]) => {
        const { valid, errors } = stringSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!test[1]) {
          checkErrorMessage(errors, "NONONO");
        }
      });
    });
  });

  describe("and", () => {
    it("should be true if every condition is true", () => {
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.and([sch => sch.minLength(1), sch => sch.maxLength(2)], "NONONO");
      const tests: Array<[string, boolean]> = [["", false], ["a", true], ["aa", true], ["aaa", false]];
      testLogger(tests, (test: [string, boolean]) => {
        const { valid, errors } = stringSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!test[1]) {
          checkErrorMessage(errors, "NONONO");
        }
      });
    });
  });

  describe("not", () => {
    it("should be true if the condition is false", () => {
      const stringSchema = new StringSchema(StringValidator);
      stringSchema.not(sch => sch.minLength(1), "NONONO");
      const tests: Array<[string, boolean]> = [["", true], ["a", false]];
      testLogger(tests, (test: [string, boolean]) => {
        const { valid, errors } = stringSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!test[1]) {
          checkErrorMessage(errors, "NONONO");
        }
      });
    });
  });
});
