import { StringSchema } from "../../src/schemas/string";
import { StringValidator } from "../../src/validators/string";
import { testLogger, Spied, typedKeys } from "../utils";
jest.mock("../../src/validators/string");

describe("StringSchema", () => {
  const spyStringValidator = StringValidator as jest.Mock<StringValidator>;
  const spies: Spied<StringValidator> = {
    is: jest.fn(() => true),
    binary: jest.fn(() => true),
    creditcard: jest.fn(() => true),
    decimal: jest.fn(() => true),
    equals: jest.fn(() => true),
    hasLowercase: jest.fn(() => true),
    hasUppercase: jest.fn(() => true),
    hasNumber: jest.fn(() => true),
    hasPunctuation: jest.fn(() => true),
    hexa: jest.fn(() => true),
    luhn: jest.fn(() => true),
    minLength: jest.fn(() => true),
    maxLength: jest.fn(() => true),
    regex: jest.fn(() => true),
    regexp: jest.fn(() => true),
  };

  beforeEach(() => {
    typedKeys(spies).forEach(key => {
      spies[key].mockClear();
      spyStringValidator.mockImplementation(() => ({
        ...spies,
      }));
    });
  });

  it("should typecheck", () => {
    spies.is.mockImplementationOnce(() => false);
    const stringSchema = new StringSchema(StringValidator, "WRONG_TYPE");
    expect(stringSchema.validate(0 as any)).toEqual({
      valid: false,
      errors: {
        "": ["WRONG_TYPE"],
      },
    });
    expect(spies.is).toHaveBeenCalledTimes(1);
  });

  it("should run every validation if the typecheck is correct", () => {
    const stringSchema = new StringSchema(StringValidator);
    stringSchema
      .minLength(1)
      .maxLength(2)
      .validate("abc");
    testLogger(["minLength", "maxLength"], (test: keyof Spied<StringValidator>) => {
      expect(spies[test]).toHaveBeenCalledTimes(1);
    });
  });

  it("should be reusable", () => {
    spies.minLength.mockImplementationOnce(() => false);
    const stringSchema = new StringSchema(StringValidator);
    stringSchema.minLength(1);
    const result1 = stringSchema.validate("ab");
    const result2 = stringSchema.validate("ab");
    expect(spies.minLength).toHaveBeenCalledTimes(2);
    expect(result1.valid).toBe(false);
    expect(result1.errors[""].length).toBe(1);
    expect(result2.valid).toBe(true);
    expect(result2.errors).toBe(undefined);
  });
});
