import { testLogger } from "../utils";
import { IErrorDict } from "../../src/interfaces";
import { NumberSchema } from "../../src/schemas/number";
import { NumberValidator } from "../../src/validators/number";

function checkErrorMessage(errors: IErrorDict, message?: string) {
  expect(errors).toBeTruthy();
  expect(errors).toHaveProperty("");
  expect(errors[""]).toBeInstanceOf(Array);
  expect(errors[""].length).toBe(1);
  if (typeof message !== "undefined") {
    expect(errors[""]).toEqual([message]);
  }
}

describe("NumberSchema", () => {
  it("should be chainable", () => {
    const tests: Array<[keyof NumberSchema, any]> = [["min", 1], ["max", 1]];
    const numberSchema = new NumberSchema(NumberValidator);
    testLogger(tests, (test: any) => {
      expect(numberSchema[test[0]](test[1])).toBe(numberSchema);
    });
  });

  it("should return a default error message", () => {
    const numberSchema = new NumberSchema(NumberValidator);
    numberSchema.equals(1);
    const { errors } = numberSchema.validate(2);
    checkErrorMessage(errors);
  });

  it("should return a custom error message", () => {
    const numberSchema = new NumberSchema(NumberValidator);
    numberSchema.equals(1, "NONONO");
    const { errors } = numberSchema.validate(2);
    checkErrorMessage(errors, "NONONO");
  });

  it("should typecheck", () => {
    const tests: any[] = ["", {}, false, true];
    testLogger(tests, test => {
      const numberSchema = new NumberSchema(NumberValidator, "NONONO");
      const { valid, errors } = numberSchema.validate(test[0]);
      expect(valid).toBe(false);
      checkErrorMessage(errors, "NONONO");
    });
  });

  describe("cast", () => {
    it("should not parse for a number if not asked", () => {
      const numberSchema = new NumberSchema(NumberValidator);
      const tests: Array<[any, boolean]> = [
        ["", false],
        ["a", false],
        ["0", false],
        ["0.1", false],
        [0, true],
        [0.1, true],
        [{}, false],
        [true, false],
        [Number.NaN, false],
      ];
      testLogger(tests, test => expect(numberSchema.validate(test[0]).valid).toBe(test[1]));
    });

    it("should give access to the cast value", () => {
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.cast();
      const tests: Array<[any, number]> = [["0", 0], ["0.1", 0.1], [0, 0], [0.1, 0.1]];
      testLogger(tests, test => expect(numberSchema.validate(test[0]).passed).toBe(test[1]));
    });

    it("should parse for a number if asked", () => {
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.cast();
      const tests: Array<[any, boolean]> = [
        ["", false],
        ["a", false],
        ["0", true],
        ["0.1", true],
        [0, true],
        [0.1, true],
        [{}, false],
        [true, false],
        [Number.NaN, false],
      ];
      testLogger(tests, test => expect(numberSchema.validate(test[0]).valid).toBe(test[1]));
    });
  });

  describe("close", () => {
    const tests: Array<[number, number, number, boolean]> = [
      [1, 0, 1, false],
      [0.9, 0, 0.9, false],
      [0.109, 0.1, 0.01, true],
      [0.11, 0.1, 0.01, true],
      [11, 10, 2, true],
      [Math.PI, 3.14, 0.1, true],
    ];
    testLogger(tests, test => {
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.close(test[1], test[2]);
      const { valid, errors } = numberSchema.validate(test[0]);
      expect(valid).toBe(test[3]);
    });
  });

  describe("equals", () => {
    it("should accept a numeric value", () => {
      const tests: Array<[number, boolean]> = [[1, true], [1.1, false]];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.equals(1);
      testLogger(tests, test => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });

    it("should do === checks", () => {
      const tests: Array<[any, any, boolean]> = [
        [0, "", false],
        [0, {}, false],
        [123, "123", false],
        ["123", 123, false],
      ];
      testLogger(tests, test => {
        const numberSchema = new NumberSchema(NumberValidator);
        numberSchema.equals(test[0]);
        expect(numberSchema.validate(test[1]).valid).toBe(test[2]);
      });
    });
  });

  describe("equalsOne", () => {
    it("should accept array value", () => {
      const tests: Array<[number, boolean]> = [[0, true], [1, false], [2, true]];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.equalsOne([0, 2]);
      testLogger(tests, test => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("integer", () => {
    it("should be true if the value is integer", () => {
      const tests: Array<[number, boolean]> = [[0, true], [0.1, false]];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.integer();
      testLogger(tests, test => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("min", () => {
    it("should be true if the number is greater or equal than...", () => {
      const tests: Array<[number, number, boolean]> = [[0, 0, true], [0, 1, false], [1, 0, true], [1, 1, true]];
      testLogger(tests, test => {
        const numberSchema = new NumberSchema(NumberValidator);
        numberSchema.min(test[1]);
        expect(numberSchema.validate(test[0]).valid).toBe(test[2]);
      });
    });
  });

  describe("max", () => {
    it("should be true if the number is less or equal than...", () => {
      const tests: Array<[number, number, boolean]> = [[0, 0, true], [0, 1, true], [1, 0, false], [1, 1, true]];
      testLogger(tests, test => {
        const numberSchema = new NumberSchema(NumberValidator);
        numberSchema.max(test[1]);
        expect(numberSchema.validate(test[0]).valid).toBe(test[2]);
      });
    });
  });

  describe("multipleOf", () => {
    it("should be true if the value is multiple of the number", () => {
      const tests: Array<[number, number, boolean]> = [
        [0, 1, true],
        [0, 2, true],
        [1, 1, true],
        [2, 1, true],
        [3, 2, false],
        [4, 2, true],
        [5, 2, false],
        [6, 2, true],
        [6, 3, true],
        [6, 4, false],
      ];
      testLogger(tests, test => {
        const numberSchema = new NumberSchema(NumberValidator);
        numberSchema.multipleOf(test[1]);
        expect(numberSchema.validate(test[0]).valid).toBe(test[2]);
      });
    });
  });

  describe("luhn", () => {
    it("should be true if the value is a Luhn mod 10 sequence", () => {
      const tests: Array<[number, boolean]> = [[79927398713, true], [79927398714, false], [4691050015091615, true]];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.luhn();
      testLogger(tests, (test: any) => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("nonnegative", () => {
    it("should be true if the value is non-negative", () => {
      const tests: Array<[number, boolean]> = [[-1, false], [0, true], [1, true]];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.nonnegative();
      testLogger(tests, (test: any) => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("prime", () => {
    it("should be true if the value is prime", () => {
      const tests: Array<[any, boolean]> = [
        ["", false],
        [0, false],
        [1, false],
        [2, true],
        [3, true],
        [4, false],
        [5, true],
        [6, false],
        [7, true],
        [8, false],
        [9, false],
        [10, false],
        [11, true],
        [13, true],
        [121, false],
        [123, false],
        [127, true],
      ];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.prime();
      testLogger(tests, (test: any) => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("positive", () => {
    it("should be true if the value is positive", () => {
      const tests: Array<[number, boolean]> = [[-1, false], [0, false], [1, true]];
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.positive();
      testLogger(tests, (test: any) => {
        expect(numberSchema.validate(test[0]).valid).toBe(test[1]);
      });
    });
  });

  describe("range", () => {
    it("should be true if the value is within the open-ended range", () => {
      const tests: Array<[number, [number | undefined, number | undefined], boolean]> = [
        [0, [0, 1], false],
        [1, [0, 1], false],
        [0.5, [0, 1], true],
        [-1, [undefined, 1], true],
        [2, [undefined, 1], false],
        [0.5, [0, undefined], true],
        [-0.5, [0, undefined], false],
        [0, [undefined, undefined], true],
      ];
      testLogger(tests, (test: any) => {
        const numberSchema = new NumberSchema(NumberValidator);
        numberSchema.range(test[1]);
        expect(numberSchema.validate(test[0]).valid).toBe(test[2]);
      });
    });
  });

  describe("regex/regexp", () => {
    it("should be true if the number matches the regex pattern", () => {
      const tests: Array<[number, RegExp, boolean]> = [
        [10, /^.*$/, true],
        [10, /^.+$/, true],
        [10, /^\w+$/, true],
        [10, /^\d+$/, true],
        [10, /^\D+$/, false],
      ];
      testLogger(tests, test => {
        const numberSchema1 = new NumberSchema(NumberValidator);
        numberSchema1.regex(test[1]);
        const numberSchema2 = new NumberSchema(NumberValidator);
        numberSchema2.regexp(test[1]);
        expect(numberSchema1.validate(test[0]).valid).toBe(test[2]);
        expect(numberSchema2.validate(test[0]).valid).toBe(test[2]);
      });
    });
  });

  describe("or", () => {
    it("should be true if some condition is true", () => {
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.or([sch => sch.equals(1), sch => sch.equals(3)], "NONONO");
      const tests: Array<[number, boolean]> = [[1, true], [2, false], [3, true]];
      testLogger(tests, test => {
        const { valid, errors } = numberSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!test[1]) {
          checkErrorMessage(errors, "NONONO");
        }
      });
    });
  });

  describe("and", () => {
    it("should be true if every condition is true", () => {
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.and([sch => sch.min(1), sch => sch.max(2)], "NONONO");
      const tests: Array<[number, boolean]> = [[0, false], [1, true], [2, true], [3, false]];
      testLogger(tests, test => {
        const { valid, errors } = numberSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!test[1]) {
          checkErrorMessage(errors, "NONONO");
        }
      });
    });
  });

  describe("not", () => {
    it("should be true if the condition is false", () => {
      const numberSchema = new NumberSchema(NumberValidator);
      numberSchema.not(sch => sch.min(1), "NONONO");
      const tests: Array<[number, boolean]> = [[0, true], [1, false]];
      testLogger(tests, test => {
        const { valid, errors } = numberSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!test[1]) {
          checkErrorMessage(errors, "NONONO");
        }
      });
    });
  });
});
