import { testLogger } from "../utils";
import { IErrorDict } from "../../src/interfaces";
import { BooleanSchema } from "../../src/schemas/boolean";
import { BooleanValidator } from "../../src/validators/boolean";

function checkErrorMessage(errors: IErrorDict, message?: string) {
  expect(errors).toBeTruthy();
  expect(errors).toHaveProperty("");
  expect(errors[""]).toBeInstanceOf(Array);
  expect(errors[""].length).toBe(1);
  if (typeof message !== "undefined") {
    expect(errors[""]).toEqual([message]);
  }
}

describe("BooleanSchema", () => {
  it("should be chainable", () => {
    const tests: Array<[keyof BooleanSchema]> = [["true"], ["false"]];
    const boolSchema = new BooleanSchema(BooleanValidator);
    testLogger(tests, (test: any) => {
      expect(boolSchema[test[0]]()).toBe(boolSchema);
    });
  });

  it("should return a default error message", () => {
    const boolSchema = new BooleanSchema(BooleanValidator);
    boolSchema.true();
    const { errors } = boolSchema.validate(false);
    checkErrorMessage(errors);
  });

  it("should return a custom error message", () => {
    const boolSchema = new BooleanSchema(BooleanValidator);
    boolSchema.true("NONONO");
    const { errors } = boolSchema.validate(false);
    checkErrorMessage(errors, "NONONO");
  });

  it("should typecheck", () => {
    const tests: any[] = ["", {}, 0, []];
    testLogger(tests, test => {
      const boolSchema = new BooleanSchema(BooleanValidator, "NONONO");
      const { valid, errors } = boolSchema.validate(test[0]);
      expect(valid).toBe(false);
      checkErrorMessage(errors, "NONONO");
    });
  });
});
