import { AnySchema } from "../../src/schemas/any";
import { NumberSchema } from "../../src/schemas/number";
import { StringSchema } from "../../src/schemas/string";
import { ArraySchema } from "../../src/schemas/array";
import { JSONSchema } from "../../src/schemas/json";
import { testLogger, numSchema, strSchema, anySchema as anySchemaFactory } from "../utils";
import { IValidateable } from "../../src/interfaces";
import { BooleanSchema } from "../../src/schemas/boolean";

describe("AnySchema", () => {
  it("gives access to TypedSchemas", () => {
    const tests: Array<[string, any]> = [
      ["number", NumberSchema],
      ["string", StringSchema],
      ["array", ArraySchema],
      ["json", JSONSchema],
      ["boolean", BooleanSchema],
    ];
    const anySchema = new AnySchema();
    testLogger(tests, test => {
      expect(anySchema).toHaveProperty(test[0]);
      expect(anySchema[test[0]]()).toBeInstanceOf(test[1]);
    });
  });

  it("does not validate if no validation is set", () => {
    const anySchema = new AnySchema();
    const { valid } = anySchema.validate(undefined);
    expect(valid).toBe(false);
  });

  describe("and/or/not", () => {
    describe("and", () => {
      it("should validate if every schema validates", () => {
        const anySchema = new AnySchema();
        anySchema.and([numSchema().min(0), numSchema().min(1), numSchema().max(2)]);
        const tests: Array<[number, boolean]> = [[-1, false], [0, false], [1, true], [2, true], [3, false]];
        testLogger(tests, test => {
          const { valid } = anySchema.validate(test[0]);
          expect(valid).toBe(test[1]);
        });
      });

      it("should validate until the first invalid schema", () => {
        const anySchema = new AnySchema();
        anySchema.and([numSchema().min(0), numSchema().min(1), numSchema().max(2)]);
        const tests: Array<[number, number]> = [[-1, 1], [0, 2], [1, 3], [2, 3], [3, 3]];
        testLogger(tests, test => {
          const { results } = anySchema.validate(test[0]);
          expect(results.length).toBe(test[1]);
        });
      });

      it("should return the errors of the sub-validations", () => {
        const anySchema = new AnySchema();
        const schemas: IValidateable[] = [numSchema().min(0), numSchema().min(1), numSchema().max(2)];
        anySchema.and(schemas, "FAIL");
        const tests: Array<[number, number]> = [[-1, 0], [0, 1], [3, 2]];
        testLogger(tests, test => {
          const expectedResults = {
            "": ["FAIL"],
            [test[1]]: schemas[test[1]].validate(test[0]).errors,
          };
          const { errors } = anySchema.validate(test[0]);
          expect(errors).toEqual(expectedResults);
        });
      });

      it("should return the results of the sub-validations", () => {
        const anySchema = new AnySchema();
        const schemas: IValidateable[] = [numSchema().min(0), numSchema().min(1), numSchema().max(2)];
        anySchema.and(schemas);
        const tests: Array<[number, number]> = [[-1, 1], [0, 2], [1, 3], [2, 3], [3, 3]];
        testLogger(tests, test => {
          const expectedResults = schemas.map(schema => schema.validate(test[0]));
          const { results } = anySchema.validate(test[0]);
          expect(results).toEqual(expectedResults.slice(0, test[1]));
        });
      });

      it("cannot be chained", () => {
        const anySchema = new AnySchema();
        const schema = strSchema();
        // @ts-ignore
        anySchema.and([schema]).not(schema);
        const tests: Array<[any, boolean]> = [[0, false], ["", true], [{}, false], [[], false]];
        testLogger(tests, test => {
          const { valid } = anySchema.validate(test[0]);
          expect(valid).toBe(test[1]);
        });
      });
    });

    describe("or", () => {
      it("should validate if at least one schema validates", () => {
        const anySchema = new AnySchema();
        anySchema.or([numSchema().min(3), numSchema().max(1), strSchema()]);
        const tests: Array<[any, boolean]> = [[0, true], [1, true], [2, false], [3, true], ["", true], [{}, false]];
        testLogger(tests, test => {
          const { valid } = anySchema.validate(test[0]);
          expect(valid).toBe(test[1]);
        });
      });

      it("should validate until the first valid schema", () => {
        const anySchema = new AnySchema();
        anySchema.or([numSchema().min(3), numSchema().max(1), strSchema()]);
        const tests: Array<[any, number]> = [[0, 2], [1, 2], [2, 3], [3, 1], ["", 3], [{}, 3]];
        testLogger(tests, test => {
          const { results } = anySchema.validate(test[0]);
          expect(results.length).toBe(test[1]);
        });
      });

      it("should return the errors of the sub-validations", () => {
        const anySchema = new AnySchema();
        const schemas: IValidateable[] = [numSchema().min(3), numSchema().max(1), strSchema()];
        anySchema.or(schemas, "FAIL");
        const tests: Array<[any, number]> = [[0, 1], [1, 1], [2, 3], ["", 2], [{}, 3]];
        testLogger(tests, test => {
          const expectedResults = {
            "": ["FAIL"],
            ...schemas.slice(0, test[1]).map(schema => schema.validate(test[0]).errors),
          };
          const { errors } = anySchema.validate(test[0]);
          expect(errors).toEqual(expectedResults);
        });
      });

      it("should return the results of the sub-validations", () => {
        const anySchema = new AnySchema();
        const schemas: IValidateable[] = [numSchema().min(3), numSchema().max(1), strSchema()];
        anySchema.or(schemas);
        const tests: Array<[any, number]> = [[0, 2], [1, 2], [2, 3], [3, 1], ["", 3], [{}, 3]];
        testLogger(tests, test => {
          const expectedResults = schemas.map(schema => schema.validate(test[0]));
          const { results } = anySchema.validate(test[0]);
          expect(results).toEqual(expectedResults.slice(0, test[1]));
        });
      });

      it("cannot be chained", () => {
        const anySchema = new AnySchema();
        const schema = strSchema();
        // @ts-ignore
        anySchema.or([schema]).not(schema);
        const tests: Array<[any, boolean]> = [[0, false], ["", true], [{}, false], [[], false]];
        testLogger(tests, test => {
          const { valid } = anySchema.validate(test[0]);
          expect(valid).toBe(test[1]);
        });
      });
    });

    describe("not", () => {
      it("should validate if the schema is invalid", () => {
        const anySchema = new AnySchema();
        anySchema.not(strSchema());
        const tests: Array<[any, boolean]> = [[0, true], ["", false], [{}, true], [[], true]];
        testLogger(tests, test => {
          const { valid } = anySchema.validate(test[0]);
          expect(valid).toBe(test[1]);
        });
      });

      it("cannot return any errors of a passed validation", () => {
        const anySchema = new AnySchema();
        const schema = strSchema();
        anySchema.not(schema, "FAIL");
        const tests: Array<[any]> = [[""], ["asdf"]];
        testLogger(tests, test => {
          const expectedResults = {
            "": ["FAIL"],
          };
          const { errors } = anySchema.validate(test[0]);
          expect(errors).toEqual(expectedResults);
        });
      });

      it("should return the results of the validation", () => {
        const anySchema = new AnySchema();
        const schema = strSchema();
        anySchema.not(schema);
        const tests: Array<[any]> = [[0], [""], [{}], [[]]];
        testLogger(tests, test => {
          const expectedResults = schema.validate(test[0]);
          const { results } = anySchema.validate(test[0]);
          expect(results).toEqual([expectedResults]);
        });
      });

      it("cannot be chained", () => {
        const anySchema = new AnySchema();
        const schema = strSchema();
        // @ts-ignore
        anySchema.not(schema).and([schema]);
        const tests: Array<[any, boolean]> = [[0, true], ["", false], [{}, true], [[], true]];
        testLogger(tests, test => {
          const { valid } = anySchema.validate(test[0]);
          expect(valid).toBe(test[1]);
        });
      });
    });

    it("should be nestable", () => {
      const schema = new AnySchema();
      schema.or([
        anySchemaFactory().and([numSchema().min(0), numSchema().max(10)]),
        anySchemaFactory().and([numSchema().min(100), numSchema().max(110)]),
      ]);
      const tests: Array<[any, boolean]> = [
        [0, true],
        [10, true],
        [11, false],
        [99, false],
        [100, true],
        [110, true],
        [111, false],
        ["", false],
        [{}, false],
      ];
      testLogger(tests, test => {
        const { valid } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });
  });
});
