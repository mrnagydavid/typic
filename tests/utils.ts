import { JSONValidator } from "../src/validators/json";
import { StringSchema } from "../src/schemas/string";
import { StringValidator } from "../src/validators/string";
import { NumberSchema } from "../src/schemas/number";
import { NumberValidator } from "../src/validators/number";
import { JSONSchema } from "../src/schemas/json";
import { ArraySchema } from "../src/schemas/array";
import { ArrayValidator } from "../src/validators/array";
import { AnySchema } from "../src/schemas/any";
import { BooleanSchema } from "../src/schemas/boolean";
import { BooleanValidator } from "../src/validators/boolean";

export function testLogger<T>(tests: T[], testFn: (test: T) => void) {
  tests.forEach(test => {
    try {
      testFn(test);
    } catch (err) {
      console.error(test);
      throw err;
    }
  });
}

export type Spied<T> = { [P in keyof T]: jest.Mock<any> };

export function typedKeys<T>(o: T): Array<keyof T> {
  // type cast should be safe because that's what really Object.keys() does
  return Object.keys(o) as Array<keyof T>;
}

export function anySchema() {
  return new AnySchema();
}

export function numSchema() {
  return new NumberSchema(NumberValidator);
}

export function strSchema() {
  return new StringSchema(StringValidator);
}

export function jsonSchema() {
  return new JSONSchema(JSONValidator);
}

export function arrSchema() {
  return new ArraySchema(ArrayValidator);
}

export function boolSchema() {
  return new BooleanSchema(BooleanValidator);
}
