import validator from "../src/index";
import { IJSON, IValidateable } from "../src/interfaces";
import { testLogger } from "./utils";

describe("API", () => {
  describe("AnySchema", () => {
    describe("and", () => {
      it("is correct", () => {
        const schema = validator().and([
          validator()
            .array()
            .each(validator().number()),
          validator()
            .json()
            .keys({
              length: validator()
                .number()
                .multipleOf(2),
            }),
        ]);
        const tests = [[[0, 1, 2], false], [[0, 1, 2, 3], true]];
        testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
      });
    });

    describe("or", () => {
      it("is correct", () => {
        const schema = validator().or([validator().string(), validator().number()]);
        const tests: Array<[any, boolean]> = [[[], false], [0, true], ["", true]];
        testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
      });
    });

    describe("not", () => {
      it("is correct", () => {
        const schema = validator().not(validator().string());
        const tests: Array<[any, boolean]> = [[[], true], [0, true], ["", false]];
        testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
      });
    });
  });

  describe("NumberSchema", () => {
    it("cast", () => {
      const withCast = validator()
        .number()
        .cast();
      const noCast = validator().number();
      const testValue: any = "0.1";
      expect(withCast.validate(testValue).valid).toBe(true);
      expect(noCast.validate(testValue).valid).toBe(false);
    });

    it("close", () => {
      const schema = validator()
        .number()
        .close(Math.PI, 0.1);
      const tests: Array<[number, boolean]> = [[3, false], [3.14, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("equals", () => {
      const schema = validator()
        .number()
        .equals(0);
      const tests: Array<[any, boolean]> = [[{}, false], [false, false], [1, false], [0, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("equalsOne", () => {
      const schema = validator()
        .number()
        .equalsOne([0, 2]);
      const tests: Array<[any, boolean]> = [[1, false], [0, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("integer", () => {
      const schema = validator()
        .number()
        .integer();
      const tests: Array<[number, boolean]> = [[0, true], [0.1, false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("luhn", () => {
      const schema = validator()
        .number()
        .luhn();
      const tests: Array<[number, boolean]> = [[4691050015091614, false], [4691050015091615, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("max", () => {
      const schema = validator()
        .number()
        .max(1);
      const tests: Array<[number, boolean]> = [[0, true], [1, true], [2, false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("min", () => {
      const schema = validator()
        .number()
        .min(1);
      const tests: Array<[number, boolean]> = [[0, false], [1, true], [2, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("multipleOf", () => {
      const schema = validator()
        .number()
        .multipleOf(3);
      const tests: Array<[number, boolean]> = [[-3, true], [0, true], [1, false], [3, true], [6, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("prime", () => {
      const schema = validator()
        .number()
        .prime();
      const tests: Array<[number, boolean]> = [[-2, false], [0, false], [1, false], [2, true], [3, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("nonnegative", () => {
      const schema = validator()
        .number()
        .nonnegative();
      const tests: Array<[number, boolean]> = [[-1, false], [0, true], [1, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("positive", () => {
      const schema = validator()
        .number()
        .positive();
      const tests: Array<[number, boolean]> = [[-1, false], [0, false], [1, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("positive", () => {
      const schema = validator()
        .number()
        .positive();
      const tests: Array<[number, boolean]> = [[-1, false], [0, false], [1, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("range #1", () => {
      const schema1 = validator()
        .number()
        .range([0, 1]);
      const tests: Array<[number, boolean]> = [[0, false], [0.5, true], [1, false]];
      testLogger(tests, test => expect(schema1.validate(test[0]).valid).toBe(test[1]));
    });

    it("range #2", () => {
      const schema2 = validator()
        .number()
        .range([undefined, 1]);
      const tests: Array<[number, boolean]> = [[0, true], [0.5, true], [1, false]];
      testLogger(tests, test => expect(schema2.validate(test[0]).valid).toBe(test[1]));
    });

    it("regex", () => {
      const schema = validator()
        .number()
        .regex(/^\d{4}$/);
      const tests: Array<[number, boolean]> = [[0, false], [123, false], [1234, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("and", () => {
      const schema1 = validator()
        .number()
        .and([sch => sch.min(0), sch => sch.max(1)]);
      const schema2 = validator()
        .number()
        .min(0)
        .max(1);
      const tests: Array<[number, boolean]> = [[0, true], [2, false]];
      testLogger(tests, test => expect(schema1.validate(test[0]).valid).toBe(test[1]));
      testLogger(tests, test => expect(schema2.validate(test[0]).valid).toBe(test[1]));
    });

    it("or", () => {
      const schema = validator()
        .number()
        .or([sch => sch.min(0).max(10), sch => sch.min(1000).max(9999)]);
      const tests: Array<[number, boolean]> = [[5, true], [55, false], [555, false], [5555, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("not", () => {
      const schema = validator()
        .number()
        .not(sch => sch.positive());
      const tests: Array<[number, boolean]> = [[-1, true], [0, true], [1, false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("optional", () => {
      const schema = validator()
        .number()
        .optional()
        .equals(0);
      const tests: Array<[number, boolean, boolean, boolean]> = [[0, true, false, true], [1, true, true, false]];
      testLogger(tests, test => {
        const { valid, errors, passed } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (test[2]) {
          expect(errors).toBeTruthy();
        } else {
          expect(errors).toBeFalsy();
        }
        expect(passed).toEqual(test[3] ? test[0] : undefined);
      });
    });
  });

  describe("StringSchema", () => {
    it("cast", () => {
      const withCast = validator()
        .string()
        .cast();
      const noCast = validator().string();
      const testValue: any = 0;
      expect(withCast.validate(testValue).valid).toBe(true);
      expect(noCast.validate(testValue).valid).toBe(false);
    });

    it("binary", () => {
      const schema = validator()
        .string()
        .binary();
      const tests: Array<[any, boolean]> = [["", false], ["0", true], ["1", true], ["2", false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("creditcard", () => {
      const schema = validator()
        .string()
        .creditcard();
      const tests: Array<[string, boolean]> = [
        ["4691050015091615", true],
        ["4691 0500 1509 1615", true],
        ["4691-0500-1509-1615", true],
        ["4691-0500-1509-1614", false],
      ];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("decimal", () => {
      const schema = validator()
        .string()
        .decimal();
      const tests: Array<[string, boolean]> = [["", false], ["0", true], ["1", true], ["9", true], ["A", false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("equals", () => {
      const schema = validator()
        .string()
        .equals("aBc");
      const tests: Array<[string, boolean]> = [["abc", false], ["aBc", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("equalsOne", () => {
      const schema = validator()
        .string()
        .equalsOne(["a", "c"]);
      const tests: Array<[string, boolean]> = [["a", true], ["b", false], ["c", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("hasLowercase", () => {
      const schema = validator()
        .string()
        .hasLowercase();
      const tests: Array<[string, boolean]> = [["a", true], ["A", false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("hasUppercase", () => {
      const schema = validator()
        .string()
        .hasUppercase();
      const tests: Array<[string, boolean]> = [["a", false], ["A", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("hasNumber", () => {
      const schema = validator()
        .string()
        .hasNumber();
      const tests: Array<[string, boolean]> = [["elite", false], ["l33t", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("hasPunctuation", () => {
      const schema = validator()
        .string()
        .hasPunctuation();
      const tests: Array<[string, boolean]> = [["Do", false], ["Do!", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("hexa", () => {
      const schema = validator()
        .string()
        .hexa();
      const tests: Array<[string, boolean]> = [
        ["", false],
        ["0", true],
        ["1", true],
        ["9", true],
        ["A", true],
        ["F", true],
        ["G", false],
      ];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("luhn", () => {
      const schema = validator()
        .string()
        .luhn();
      const tests: Array<[string, boolean]> = [
        ["79927398713", true],
        ["79927398714", false],
        ["4691x0500x1509x1615", true],
        ["4691-0500-1509-1615", true],
      ];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("minLength", () => {
      const schema = validator()
        .string()
        .minLength(1);
      const tests: Array<[string, boolean]> = [["", false], ["a", true], ["aa", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("maxLength", () => {
      const schema = validator()
        .string()
        .maxLength(1);
      const tests: Array<[string, boolean]> = [["", true], ["a", true], ["aa", false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("regex", () => {
      const schema = validator()
        .number()
        .regex(/^\d{4}$/);
      const tests: Array<[number, boolean]> = [[0, false], [123, false], [1234, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("and", () => {
      const schema1 = validator()
        .string()
        .and([sch => sch.minLength(0), sch => sch.maxLength(1)]);
      const schema2 = validator()
        .string()
        .minLength(0)
        .maxLength(1);
      const tests: Array<[string, boolean]> = [["a", true], ["aa", false]];
      testLogger(tests, test => expect(schema1.validate(test[0]).valid).toBe(test[1]));
      testLogger(tests, test => expect(schema2.validate(test[0]).valid).toBe(test[1]));
    });

    it("or", () => {
      const schema = validator()
        .string()
        .or([sch => sch.minLength(1).maxLength(2), sch => sch.minLength(4).maxLength(4)]);
      const tests: Array<[string, boolean]> = [["84", true], ["984", false], ["1984", true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("not", () => {
      const schema = validator()
        .string()
        .not(sch => sch.hasNumber());
      const tests: Array<[string, boolean]> = [["elite", true], ["l33t", false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("optional", () => {
      const schema = validator()
        .string()
        .optional()
        .equals("ok");
      const tests: Array<[string, boolean, boolean, boolean]> = [
        ["ok", true, false, true],
        ["not ok", true, true, false],
      ];
      testLogger(tests, test => {
        const { valid, errors, passed } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (test[2]) {
          expect(errors).toBeTruthy();
        } else {
          expect(errors).toBeFalsy();
        }
        expect(passed).toEqual(test[3] ? test[0] : undefined);
      });
    });
  });

  describe("BooleanSchema", () => {
    it("cast", () => {
      const withCast = validator()
        .boolean()
        .cast();
      const noCast = validator().boolean();
      const testValue: any = "truthy";
      expect(withCast.validate(testValue).valid).toBe(true);
      expect(noCast.validate(testValue).valid).toBe(false);
    });

    it("true", () => {
      const schema = validator()
        .boolean()
        .true();
      const tests: Array<[any, boolean]> = [["", false], ["0", false], [false, false], [true, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("false", () => {
      const schema = validator()
        .boolean()
        .false();
      const tests: Array<[any, boolean]> = [["", false], ["0", false], [false, true], [true, false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("and", () => {
      const trueSchema = validator()
        .boolean()
        .and([sch => sch.true(), sch => sch.true()]);
      const neverSchema = validator()
        .boolean()
        .and([sch => sch.true(), sch => sch.false()]);
      const falseSchema = validator()
        .boolean()
        .and([sch => sch.false(), sch => sch.false()]);
      const tests: Array<[any, boolean, boolean, boolean]> = [[true, true, false, false], [false, false, false, true]];
      testLogger(tests, test => expect(trueSchema.validate(test[0]).valid).toBe(test[1]));
      testLogger(tests, test => expect(neverSchema.validate(test[0]).valid).toBe(test[2]));
      testLogger(tests, test => expect(falseSchema.validate(test[0]).valid).toBe(test[3]));
    });

    it("or", () => {
      const trueSchema = validator()
        .boolean()
        .or([sch => sch.true(), sch => sch.true()]);
      const alwaysSchema = validator()
        .boolean()
        .or([sch => sch.true(), sch => sch.false()]);
      const falseSchema = validator()
        .boolean()
        .or([sch => sch.false(), sch => sch.false()]);
      const tests: Array<[any, boolean, boolean, boolean]> = [[true, true, true, false], [false, false, true, true]];
      testLogger(tests, test => expect(trueSchema.validate(test[0]).valid).toBe(test[1]));
      testLogger(tests, test => expect(alwaysSchema.validate(test[0]).valid).toBe(test[2]));
      testLogger(tests, test => expect(falseSchema.validate(test[0]).valid).toBe(test[3]));
    });

    it("not", () => {
      const trueSchema = validator()
        .boolean()
        .not(sch => sch.false());
      const falseSchema = validator()
        .boolean()
        .not(sch => sch.true());
      const tests: Array<[any, boolean, boolean]> = [[true, true, false], [false, false, true]];
      testLogger(tests, test => expect(trueSchema.validate(test[0]).valid).toBe(test[1]));
      testLogger(tests, test => expect(falseSchema.validate(test[0]).valid).toBe(test[2]));
    });

    it("optional", () => {
      const schema = validator()
        .boolean()
        .true()
        .optional();
      const tests: Array<[any, boolean]> = [["", true], ["0", true], [false, true], [true, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });
  });

  describe("ArraySchema", () => {
    it("each #1", () => {
      const schema1 = validator()
        .array()
        .each(
          validator()
            .string()
            .minLength(1)
        );
      const tests: Array<[string[], boolean]> = [[["a", ""], false], [["a", "b"], true]];
      testLogger(tests, test => expect(schema1.validate(test[0]).valid).toBe(test[1]));
    });

    it("each #1", () => {
      const schema2 = validator()
        .array()
        .each((elem, index) =>
          validator()
            .string()
            .minLength(index)
            .validate(elem)
        );
      const tests: Array<[string[], boolean]> = [[["", "b", "c"], false], [["", "b", "cc"], true]];
      testLogger(tests, test => expect(schema2.validate(test[0]).valid).toBe(test[1]));
    });

    it("maxLength", () => {
      const schema = validator()
        .array()
        .maxLength(1);
      const tests: Array<[string[], boolean]> = [[[], true], [["a"], true], [["a", "a"], false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("minLength", () => {
      const schema = validator()
        .array()
        .minLength(1);
      const tests: Array<[string[], boolean]> = [[[], false], [["a"], true], [["a", "a"], true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("length", () => {
      const schema = validator()
        .array()
        .length(1);
      const tests: Array<[string[], boolean]> = [[[], false], [["a"], true], [["a", "a"], false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("value", () => {
      const schema = validator()
        .array()
        .value(1, validator().number());
      const tests: Array<[any[], boolean]> = [[["a", "a", "a"], false], [["a", 1, "a"], true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("values", () => {
      const schema = validator()
        .array()
        .values([validator().number(), validator().string()]);
      const tests: Array<[any[], boolean]> = [[["a", 1], false], [[1, "a"], true], [[1, "a", 2], true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("optional", () => {
      const schema = validator()
        .array()
        .optional()
        .minLength(1);
      const tests: Array<[any[], boolean, boolean, boolean]> = [[["ok"], true, false, true], [[], true, true, false]];
      testLogger(tests, test => {
        const { valid, errors, passed } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (test[2]) {
          expect(errors).toBeTruthy();
        } else {
          expect(errors).toBeFalsy();
        }
        expect(passed).toEqual(test[3] ? test[0] : undefined);
      });
    });
  });

  describe("JSONSchema", () => {
    it("allowedKeys", () => {
      const schema = validator()
        .json()
        .allowedKeys(["bar"]);
      const tests: Array<[any, boolean]> = [[{ foo: "bar" }, false], [{ bar: "foo" }, true]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("each", () => {
      const schema = validator()
        .json()
        .each(
          validator()
            .string()
            .minLength(3)
        );
      const tests: Array<[any, boolean]> = [[{ foo: "bar", bar: "foo" }, true], [{ foo: "bar", bar: 3 }, false]];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("keys", () => {
      const schema = validator()
        .json()
        .keys({
          foo: validator()
            .array()
            .each(validator().string()),
        });
      const tests: Array<[any, boolean]> = [
        [{ foo: "bar" }, false],
        [{ foo: ["bar"] }, true],
        [{ foo: ["bar"], boo: "bah" }, true],
      ];
      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });

    it("optional", () => {
      const schema = validator()
        .json()
        .optional()
        .allowedKeys(["foo"]);
      const tests: Array<[any, boolean, boolean, boolean]> = [
        [{ foo: "ok" }, true, false, true],
        [{ foo: "ok", bar: "not okay" }, true, true, false],
      ];
      testLogger(tests, test => {
        const { valid, errors, passed } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (test[2]) {
          expect(errors).toBeTruthy();
        } else {
          expect(errors).toBeFalsy();
        }
        expect(passed).toEqual(test[3] ? test[0] : undefined);
      });
    });
  });

  describe("ISimpleResult", () => {
    it("passed field demo", () => {
      const withCast = validator()
        .number("Not a number!")
        .cast();
      const noCast = validator().number("Not a number!");
      const testValue: any = "0";

      const castResult = withCast.validate(testValue);
      expect(castResult.valid).toBe(true);
      expect(castResult.errors).toEqual(undefined);
      expect(castResult.passed).toEqual(0);

      const noCastResult = noCast.validate(testValue);
      expect(noCastResult.valid).toBe(false);
      expect(noCastResult.errors).toEqual({ "": ["Not a number!"] });
      expect(noCastResult.passed).toEqual(undefined);
    });
  });

  describe("IComplexResult", () => {
    it("passed field demo", () => {
      const schema = validator()
        .json("Not a JSON!")
        .keys(
          {
            foo: validator()
              .string("Not a string!")
              .minLength(4, "Min 4 characters!")
              .decimal("Only numbers!"),
          },
          "Key check failed!"
        );

      let result;
      result = schema.validate(0 as any);
      expect(result.valid).toBe(false);
      expect(result.errors).toEqual({ "": ["Not a JSON!"] });
      expect(result.passed).toEqual(undefined);

      result = schema.validate({} as any);
      expect(result.valid).toBe(false);
      expect(result.errors).toEqual({ "": ["Key check failed!"], foo: { "": ["Not a string!"] } });
      expect(result.passed).toEqual({});

      result = schema.validate({ foo: "bar" } as any);
      expect(result.valid).toBe(false);
      expect(result.errors).toEqual({ "": ["Key check failed!"], foo: { "": ["Min 4 characters!", "Only numbers!"] } });
      expect(result.passed).toEqual({});

      result = schema.validate({ foo: "bara" } as any);
      expect(result.valid).toBe(false);
      expect(result.errors).toEqual({ "": ["Key check failed!"], foo: { "": ["Only numbers!"] } });
      expect(result.passed).toEqual({});

      result = schema.validate({ foo: "123" } as any);
      expect(result.valid).toBe(false);
      expect(result.errors).toEqual({ "": ["Key check failed!"], foo: { "": ["Min 4 characters!"] } });
      expect(result.passed).toEqual({});

      result = schema.validate({ foo: "1234" } as any);
      expect(result.valid).toBe(true);
      expect(result.errors).toEqual(undefined);
      expect(result.passed).toEqual({ foo: "1234" });
    });
  });
});

describe("COOKBOOK", () => {
  /*
  describe("tests on the webpage", () => {
    it("array examples", () => {
      const schema = validator()
        .array()
        .minLength(2)
        .each(
          validator()
            .number()
            .min(0)
            .max(10)
        );
      const tests: any[] = [{}, [], [0, -2, 1], [0, 2, 1]];
      testLogger(tests, test => {
        const result = schema.validate(test);
        console.log(JSON.stringify(result, null, 2));
      });
    });

    it("json examples", () => {
      const schema = validator()
        .json()
        .keys({
          email: validator()
            .string()
            .regex(/\@/), // I know, I know...
          password: validator()
            .string()
            .minLength(8)
            .hasUppercase(),
        });
      const tests: any[] = [
        0,
        [],
        {},
        {
          email: "a@b.hu",
          password: "abcd",
        },
        {
          email: "a@b.hu",
          password: "Password1",
        },
      ];
      testLogger(tests, test => {
        const result = schema.validate(test);
        console.log(JSON.stringify(result, null, 2));
      });
    });
  });
  */
  describe("custom error codes demo", () => {
    it("can return any fail message (primitive)", () => {
      const schema = validator()
        .string({ code: 1001, message: "Not a string!" })
        .minLength(1, { code: 1002, message: "It should be at least 1 characters long!" })
        .maxLength(2, { code: 1003, message: "It shouldn't be more than 2 characters long!" });

      const tests: Array<[any, boolean, any]> = [
        [0, false, { code: 1001, message: "Not a string!" }],
        ["", false, { code: 1002, message: "It should be at least 1 characters long!" }],
        ["aaa", false, { code: 1003, message: "It shouldn't be more than 2 characters long!" }],
        ["a", true, {}],
      ];

      testLogger(tests, test => {
        const { valid, errors } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!valid) {
          expect(errors[""]).toEqual([test[2]]);
        }
      });
    });

    it("can return multiple fail messages (primitive)", () => {
      const schema = validator()
        .string()
        .minLength(8, { code: 1000, message: "Password should be at least 8 characters long!" })
        .maxLength(64, { code: 1001, message: "Password should not be more than 64 characters long!" })
        .hasNumber({ code: 1002, message: "Password should contain numbers!" })
        .hasUppercase({ code: 1003, message: "Password should contain uppercase letters!" });
      const { valid, errors } = schema.validate("x");
      expect(valid).toBe(false);
      expect(errors).toEqual({
        "": [
          { code: 1000, message: "Password should be at least 8 characters long!" },
          { code: 1002, message: "Password should contain numbers!" },
          { code: 1003, message: "Password should contain uppercase letters!" },
        ],
      });
    });

    it("can return any fail message (json)", () => {
      const strSchema = validator()
        .string({ code: 1001, message: "Not a string!" })
        .minLength(1, { code: 1002, message: "It should be at least 1 characters long!" })
        .maxLength(2, { code: 1003, message: "It shouldn't be more than 2 characters long!" });
      const schema = validator()
        .json({ code: 2000, message: "Not a JSON!" })
        .keys(
          {
            name: strSchema,
          },
          { code: 2001, message: "JSON doesn't match required format!" }
        );

      const tests: Array<[any, boolean, any]> = [
        [0, false, { "": [{ code: 2000, message: "Not a JSON!" }] }],
        [
          {},
          false,
          {
            "": [{ code: 2001, message: "JSON doesn't match required format!" }],
            name: { "": [{ code: 1001, message: "Not a string!" }] },
          },
        ],
        [
          { name: "" },
          false,
          {
            "": [{ code: 2001, message: "JSON doesn't match required format!" }],
            name: { "": [{ code: 1002, message: "It should be at least 1 characters long!" }] },
          },
        ],
        [
          { name: "aaa" },
          false,
          {
            "": [{ code: 2001, message: "JSON doesn't match required format!" }],
            name: { "": [{ code: 1003, message: "It shouldn't be more than 2 characters long!" }] },
          },
        ],
        [{ name: "aa" }, true, {}],
      ];

      testLogger(tests, test => {
        const { valid, errors } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
        if (!valid) {
          expect(errors).toEqual(test[2]);
        }
      });
    });
  });

  describe("function argument type checking demo", () => {
    it("can validate union types", () => {
      /*
        function foo(bar: string | number) { ...
      */
      const barSchema = validator().or([validator().string(), validator().number()]);

      const tests: Array<[any, boolean]> = [[0, true], ["", true], [{}, false]];
      testLogger(tests, test => {
        const { valid } = barSchema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });
  });

  describe("ENUM demo", () => {
    it("can validate enums", () => {
      enum Yoda {
        DO,
        DO_NOT,
      }

      const schema = validator()
        .string()
        .equalsOne(Object.keys(Yoda))
        .not(sch => sch.decimal());

      const tests: Array<[string, boolean]> = [
        ["0", false],
        ["1", false],
        ["DO", true],
        ["DO_NOT", true],
        ["ANY", false],
      ];

      testLogger(tests, test => expect(schema.validate(test[0]).valid).toBe(test[1]));
    });
  });

  describe("Array demo", () => {
    it("can validate that each element in the array is as expected", () => {
      const schema = validator().string();
      const arraySchema = validator()
        .array()
        .each(schema);
      const tests: Array<[any[], boolean]> = [
        [[], true],
        [[0], false],
        [[""], true],
        [["", 0], false],
        [["", ""], true],
      ];
      testLogger(tests, test => {
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });

    it("can validate elements by a pattern", () => {
      const schemaPattern: IValidateable[] = [validator().string(), validator().number(), validator().number()];
      const validatorFn = (elem: any, index: number) => schemaPattern[index % 3].validate(elem);
      const arraySchema = validator()
        .array()
        .each(validatorFn);
      const tests: Array<[any[], boolean]> = [
        [[], true],
        [[0], false],
        [[""], true],
        [["", ""], false],
        [["", 0], true],
        [["", 0, ""], false],
        [["", 0, 0], true],
        [["", 0, 0, 0], false],
        [["", 0, 0, ""], true],
      ];
      testLogger(tests, test => {
        const { valid } = arraySchema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });

    it("can validate elements by a fix-length pattern", () => {
      /*
        Useful with rest arguments.
      */
      const schemaPattern: IValidateable[] = [validator().string(), validator().number(), validator().number()];
      const validatorFn = (elem: any, index: number, arr: any[]) =>
        schemaPattern[index % schemaPattern.length].validate(elem);
      const arraySchema = validator()
        .array()
        .each(validatorFn);
      const jsonSchema = validator()
        .json()
        .keys({
          length: validator()
            .number()
            .min(3)
            .multipleOf(3),
        });
      const schema = validator().and([jsonSchema, arraySchema]);
      const tests: Array<[any[], boolean]> = [
        [[], false],
        [[0], false],
        [[""], false],
        [["", ""], false],
        [["", 0], false],
        [["", 0, ""], false],
        [["", 0, 0], true],
        [["", 0, 0, 0], false],
        [["", 0, 0, ""], false],
        [["", 0, 0, "", 0], false],
        [["", 0, 0, "", 0, 0], true],
      ];
      testLogger(tests, test => {
        const { valid } = schema.validate(test[0]);
        expect(valid).toBe(test[1]);
      });
    });
  });

  describe("JSON demo", () => {
    describe("required/full evaluation", () => {
      it("can be used to allow only a predefined set of fields", () => {
        const schema = validator()
          .json()
          .allowedKeys(["email", "password", "name", "age", "location"]);

        // [test object, validity, keys available in the result]
        const tests: Array<[IJSON, boolean, string[]]> = [
          [{}, true, []],
          [{ email: "a@b.hu" }, true, ["email"]],
          [{ password: "Almafa123" }, true, ["password"]],
          [{ email: "a@b.hu", password: "Almafa123" }, true, ["email", "password"]],
          [{ email: "a@b.hu", password: "Almafa123", name: "D" }, true, ["email", "password", "name"]],
          [{ email: "a@b.hu", password: "Almafa123", name: "D", age: 14 }, true, ["email", "password", "name", "age"]],
          [
            { email: "a@b.hu", password: "Almafa123", name: "D", age: 14, location: "HU" },
            true,
            ["email", "password", "name", "age", "location"],
          ],
          [
            { email: "a@b.hu", password: "Almafa123", name: "D", age: 14, location: "HU", foo: "bar" },
            false,
            ["email", "password", "name", "age", "location"],
          ],
        ];

        testLogger(tests, test => {
          const { valid, passed } = schema.validate(test[0]);
          expect(valid).toBe(test[1]);
          expect(Object.keys(passed)).toEqual(test[2]);
        });
      });

      it("can be used to filter invalid optional fields", () => {
        /*
          Use two schemas to filter valid fields and enforce presence of minimum required fields!
          Fields that are not validated by a schema are also passed on (see: foo: "bar").
        */
        const allowedFields = ["email", "password", "name", "age", "location"];
        const email = validator()
          .string()
          .regex(/\@/);
        const password = validator()
          .string()
          .minLength(8)
          .maxLength(64)
          .hasLowercase()
          .hasUppercase()
          .hasNumber();
        const name = validator().string();
        const age = validator()
          .number()
          .min(13)
          .max(99);
        const location = validator().string();
        const fullSchema = validator()
          .json()
          .allowedKeys(allowedFields)
          .keys({
            email,
            password,
            name,
            age,
            location,
          });
        const requiredSchema = validator()
          .json()
          .keys({
            email,
            password,
          });

        // [test object, isValid for full schema, isValid for required schema, keys available in the result]
        const tests: Array<[IJSON, boolean, boolean, string[]]> = [
          [{}, false, false, []],
          [{ email: "a" }, false, false, []],
          [{ email: "a@b.hu" }, false, false, ["email"]],
          [{ password: "Almafa123" }, false, false, ["password"]],
          [{ email: "a@b.hu", password: "Almafa123" }, false, true, ["email", "password"]],
          [{ email: "a@b.hu", password: "Almafa123", name: "D" }, false, true, ["email", "password", "name"]],
          [{ email: "a@b.hu", password: "Almafa123", name: "D", age: 5 }, false, true, ["email", "password", "name"]],
          [
            { email: "a@b.hu", password: "Almafa123", name: "D", age: 14 },
            false,
            true,
            ["email", "password", "name", "age"],
          ],
          [
            { email: "a@b.hu", password: "Almafa123", name: "D", age: 14, location: "HU" },
            true,
            true,
            ["email", "password", "name", "age", "location"],
          ],
          [
            { email: "a@b.hu", password: "Almafa123", name: "D", age: 14, location: "HU", foo: "bar" },
            false,
            true,
            ["email", "password", "name", "age", "location"],
          ],
        ];

        testLogger(tests, test => {
          const { valid, passed } = fullSchema.validate(test[0]);
          expect(valid).toBe(test[1]);
          const result = requiredSchema.validate(passed);
          expect(result.valid).toBe(test[2]);
          expect(Object.keys(result.passed)).toEqual(test[3]);
        });
      });

      it("can validate optional fields", () => {
        const schema = validator()
          .json()
          .keys({
            email: validator()
              .string()
              .regex(/@/),
            name: validator()
              .string("Should be a string!")
              .optional()
              .minLength(5, "Not long enough!"),
          });
        const tests: Array<[any, boolean, any, any?]> = [
          [{ email: "a@.c" }, true, { email: "a@.c" }, { name: { "": ["Should be a string!"] } }],
          [{ email: "a@.c", name: "The Name" }, true, { email: "a@.c", name: "The Name" }],
          [{ name: "The Name" }, false, { name: "The Name" }],
          [{ email: "a@.c", name: "Name" }, true, { email: "a@.c" }, { name: { "": ["Not long enough!"] } }],
        ];
        testLogger(tests, test => {
          const { valid, errors, passed } = schema.validate(test[0]);
          expect(valid).toBe(test[1]);
          expect(passed).toEqual(test[2]);
          if (test[3]) {
            expect(errors).toEqual(test[3]);
          }
        });
      });

      it("can validate optional indices", () => {
        const schema = validator()
          .array()
          .value(
            0,
            validator()
              .string()
              .optional()
              .minLength(2)
          )
          .value(
            1,
            validator()
              .string()
              .optional()
              .minLength(2)
          );
        const tests: Array<[any[], boolean, any[]]> = [
          [[], true, []],
          [["a"], true, [undefined]],
          [["aa"], true, ["aa"]],
          [["aa", ""], true, ["aa", undefined]],
          [["aa", "a"], true, ["aa", undefined]],
          [["aa", "aa"], true, ["aa", "aa"]],
        ];
        testLogger(tests, test => {
          const { valid, passed } = schema.validate(test[0]);
          expect(valid).toBe(test[1]);
          expect(passed).toEqual(test[2]);
        });
      });

      it("can validate mapped types", () => {
        const userSchema = validator()
          .json()
          .keys({
            name: validator()
              .string()
              .minLength(1)
              .maxLength(20),
            password: validator()
              .string()
              .minLength(8)
              .hasUppercase()
              .hasNumber(),
          });
        const userListSchema = validator()
          .json()
          .each(userSchema);

        const result = userListSchema.validate({
          "user1@domain.com": {
            name: "Justin Beaver",
            password: "Password1",
          },
          "user2@domain.com": {
            name: "Gargamel",
            password: "N05murf5",
          },
        });

        expect(result.valid).toBe(true);
      });
    });
  });
});
