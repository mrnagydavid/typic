- [AnySchema](#anyschema)
  - [AnySchema.number](#anyschemanumber)
  - [AnySchema.string](#anyschemastring)
  - [AnySchema.array](#anyschemaarray)
  - [AnySchema.json](#anyschemajson)
  - [AnySchema.and](#anyschemaand)
  - [AnySchema.or](#anyschemaor)
  - [AnySchema.not](#anyschemanot)
- [NumberSchema](#numberschema)
  - [NumberSchema.cast](#numberschemacast)
  - [NumberSchema.close](#numberschemaclose)
  - [NumberSchema.equals](#numberschemaequals)
  - [NumberSchema.equalsOne](#numberschemaequalsone)
  - [NumberSchema.integer](#numberschemainteger)
  - [NumberSchema.luhn](#numberschemaluhn)
  - [NumberSchema.max](#numberschemamax)
  - [NumberSchema.min](#numberschemamin)
  - [NumberSchema.multipleOf](#numberschemamultipleof)
  - [NumberSchema.nonnegative](#numberschemanonnegative)
  - [NumberSchema.positive](#numberschemapositive)
  - [NumberSchema.prime](#numberschemaprime)
  - [NumberSchema.range](#numberschemarange)
  - [NumberSchema.regex](#numberschemaregex)
  - [NumberSchema.and](#numberschemaand)
  - [NumberSchema.or](#numberschemaor)
  - [NumberSchema.not](#numberschemanot)
  - [NumberSchema.optional](#numberschemaoptional)
- [StringSchema](#stringschema)
  - [StringSchema.cast](#stringschemacast)
  - [StringSchema.binary](#stringschemabinary)
  - [StringSchema.creditcard](#stringschemacreditcard)
  - [StringSchema.decimal](#stringschemadecimal)
  - [StringSchema.equals](#stringschemaequals)
  - [StringSchema.equalsOne](#stringschemaequalsone)
  - [StringSchema.hasLowercase](#stringschemahaslowercase)
  - [StringSchema.hasUppercase](#stringschemahasuppercase)
  - [StringSchema.hasNumber](#stringschemahasnumber)
  - [StringSchema.hasPunctuation](#stringschemahaspunctuation)
  - [StringSchema.hexa](#stringschemahexa)
  - [StringSchema.luhn](#stringschemaluhn)
  - [StringSchema.minLength](#stringschemaminlength)
  - [StringSchema.maxLength](#stringschemamaxlength)
  - [StringSchema.regex](#stringschemaregex)
  - [StringSchema.and](#stringschemaand)
  - [StringSchema.or](#stringschemaor)
  - [StringSchema.not](#stringschemanot)
  - [StringSchema.optional](#stringschemaoptional)
- [BooleanSchema](#booleanschema)
  - [BooleanSchema.cast](#booleanschemacast)
  - [BooleanSchema.true](#booleanschematrue)
  - [BooleanSchema.false](#booleanschemafalse)
  - [BooleanSchema.and](#booleanschemaand)
  - [BooleanSchema.or](#booleanschemaor)
  - [BooleanSchema.not](#booleanschemanot)
  - [BooleanSchema.optional](#booleanschemaoptional)
- [ArraySchema](#arrayschema)
  - [ArraySchema.each](#arrayschemaeach)
  - [ArraySchema.maxLength](#arrayschemamaxlength)
  - [ArraySchema.minLength](#arrayschemaminlength)
  - [ArraySchema.length](#arrayschemalength)
  - [ArraySchema.value](#arrayschemavalue)
  - [ArraySchema.values](#arrayschemavalues)
  - [ArraySchema.optional](#arrayschemaoptional)
- [JSONSchema](#jsonschema)
  - [JSONSchema.allowedKeys](#jsonschemaallowedkeys)
  - [JSONSchema.each](#jsonschemaeach)
  - [JSONSchema.keys](#jsonschemakeys)
  - [JSONSchema.optional](#jsonschemaoptional)
- [Misc](#misc)
  - [Types](#types)
    - [ErrorItem](#erroritem)
    - [ErrorList](#errorlist)
    - [ValidatorFn](#validatorfn)
  - [Interfaces](#interfaces)
    - [IErrorDict](#ierrordict)
    - [IErrorJSON](#ierrorjson)
    - [IValidateable](#ivalidateable)
    - [ILockedAnySchema](#ilockedanyschema)
    - [IResult](#iresult)
    - [ILogicResult](#ilogicresult)
    - [ISimpleResult](#isimpleresult)
    - [IComplexResult](#icomplexresult)

```typescript
import validator from "typic";

const schema = validator() // --> AnySchema
  .number() // Schema conditions are chained
  .min(0) // Chains are mutable
  .max(10);

schema.validate(10);
```

# AnySchema

Calling the validator factory function imported from "typic" returns an AnySchema. It is a junction point where the expected type is decided. Apart from being able to choose a type, the AnySchema provides some basic logical connecting of different schemas.

Methods:

- [and](#anyschemaand)(options: [IValidateable[]](#ivalidateable), fail?: [ErrorItem](#erroritem)): [ILockedAnySchema](#ilockedanyschema)
- [or](#anyschemaor)(options: [IValidateable[]](#ivalidateable), fail?: [ErrorItem](#erroritem)): [ILockedAnySchema](#ilockedanyschema)
- [not](#anyschemanot)(condition: [IValidateable](#ivalidateable), fail?: [ErrorItem](#erroritem)): [ILockedAnySchema](#ilockedanyschema)
- [number](#anyschemanumber)(fail?: [ErrorItem](#erroritem)): [NumberSchema](#numberschema)
- [string](#anyschemastring)(fail?: [ErrorItem](#erroritem)): [StringSchema](#stringschema)
- [array](#anyschemaarray)(fail?: [ErrorItem](#erroritem)): [ArraySchema](#arrayschema)
- [json](#anyschemajson)(fail?: [ErrorItem](#erroritem)): [JSONSchema](#jsonschema)
- validate(value: any): [ILogicResult](#ilogicresult)

## AnySchema.number

```typescript
number(fail?: ErrorItem): NumberSchema;
```

Returns a [NumberSchema](#numberschema).

## AnySchema.string

```typescript
string(fail?: ErrorItem): StringSchema;
```

Returns a [StringSchema](#stringschema).

## AnySchema.array

```typescript
array(fail?: ErrorItem): ArraySchema;
```

Returns an [ArraySchema](#arrayschema).

## AnySchema.json

```typescript
json(fail?: ErrorItem): JSONSchema;
```

Returns a [JSONSchema](#jsonschema).

## AnySchema.and

```typescript
and(options: IValidateable[], fail?: ErrorItem): ILockedAnySchema;
```

The schema validates only if every IValidateable in `options` validate.

The following example checks if an array contains even-numbered elements and each of them are numbers.

```typescript
const schema = validator().and([
  validator()
    .array()
    .each(validator().number()),
  validator()
    .json()
    .keys({
      length: validator()
        .number()
        .multipleOf(2),
    }),
]);
schema.validate([0, 1, 2]); // Fails
schema.validate([0, 1, 2, 3]); // Passes
```

## AnySchema.or

```typescript
or(options: IValidateable[], fail?: ErrorItem): ILockedAnySchema;
```

The schema validates only if at least one IValidateable in `options` validate.

The following example checks if a value is either string or number.

```typescript
const schema = validator().or([validator().string(), validator().number()]);
schema.validate([]); // Fails
schema.validate(0); // Passes
schema.validate(""); // Passes
```

## AnySchema.not

```typescript
not(option: IValidateable, fail?: ErrorItem): ILockedAnySchema;
```

The schema validates only if the IValidateable in `options` does not validate.

The following example makes sure the value is not a string.

```typescript
const schema = validator().not(validator().string());
schema.validate([]); // Passes
schema.validate(0); // Passes
schema.validate(""); // Fails
```

# NumberSchema

A NumberSchema by itself validates that a value is of type `number`. It accepts everything with a `typeof` value `"number"`, that is also finite and is _NOT_ a `NaN`.

Apart from the basic typechecking the NumberSchema provides some utilities to check other properties of the numeric value.

- [cast](#numberschemacast): NumberSchema;
- [close](#numberschemaclose)(num: number, epsilon: number, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [equals](#numberschemaequals)(num: number, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [equalsOne](#numberschemaequalsOne)(nums: number[], fail?: [ErrorItem](#erroritem)): NumberSchema;
- [integer](#numberschemainteger)(fail?: [ErrorItem](#erroritem)): NumberSchema;
- [luhn](#numberschemaluhn)(fail?: [ErrorItem](#erroritem)): NumberSchema;
- [max](#numberschemamax)(ceil: number, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [min](#numberschemamin)(floor: number, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [multipleOf](#numberschemamultipleOf)(num: number, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [nonnegative](#numberschemanonnegative)(fail?: [ErrorItem](#erroritem)): NumberSchema;
- [positive](#numberschemapositive)(fail?: [ErrorItem](#erroritem)): NumberSchema;
- [prime](#numberschemaprime)(fail?: [ErrorItem](#erroritem)): NumberSchema;
- [range](#numberschemarange)(bounds: [number | undefined, number | undefined], fail?: [ErrorItem](#erroritem)): NumberSchema;
- [regex](#numberschemaregex)(pattern: RegExp, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [regexp](#numberschemaregex)(pattern: RegExp, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [and](#numberschemaand)(options: Array<(sch: NumberSchema) => NumberSchema>, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [or](#numberschemaor)(options: Array<(sch: NumberSchema) => NumberSchema>, fail?: [ErrorItem](#erroritem)): NumberSchema;
- [not](#numberschemanot)(condition: (sch: NumberSchema) => NumberSchema, fail?: [ErrorItem](#erroritem)): NumberSchema;
- validate(value: number): [ISimpleResult](#isimpleresult)

## NumberSchema.cast

```typescript
cast(): NumberSchema
```

If the value is a string, the validator attempts to run it against `Number.parseFloat` first. The parsed value is accessible through the [validation result object's](#isimpleresult) `passed` field.

```typescript
const withCast = validator()
  .number()
  .cast();
const noCast = validator().number();

withCast.validate("0.1"); // Passes
noCast.validate("0.1"); // Fails
```

## NumberSchema.close

```typescript
close(
  num: number,
  epsilon: number,
  fail?: ErrorItem,
): NumberSchema
```

Validates only if the value is within an open ended `epsilon` radius range of `num`.

```typescript
const schema = validator()
  .number()
  .close(Math.PI, 0.1);
schema.validate(3); // Fails
schema.validate(3.14); // Passes
```

## NumberSchema.equals

```typescript
equals(num: number, fail?: ErrorItem): NumberSchema
```

Validates only if the value equals (`===`) to `num`.

```typescript
const schema = validator()
  .number()
  .equals(0);
schema.validate({}); // Fails
schema.validate(false); // Fails
schema.validate(1); // Fails
schema.validate(0); // Passes
```

## NumberSchema.equalsOne

```typescript
equalsOne(matches: number[], fail?: ErrorItem): NumberSchema
```

Validates only if the value equals (`===`) to at least one of the `matches`.

```typescript
const schema = validator()
  .number()
  .equalsOne([0, 2]);
schema.validate(1); // Fails
schema.validate(0); // Passes
```

## NumberSchema.integer

```typescript
integer(fail?: ErrorItem): NumberSchema
```

Validates only if the value is an integer number.

```typescript
const schema = validator()
  .number()
  .integer();
schema.validate(0.1); // Fails
schema.validate(0); // Passes
```

## NumberSchema.luhn

```typescript
luhn(fail?: ErrorItem): NumberSchema
```

Validates only if the number sequence of the value conforms to the [Luhn formula](https://en.wikipedia.org/wiki/Luhn_algorithm). This formula can be used to check the syntactic validity of credit card or IMEI numbers.

```typescript
const schema = validator()
  .number()
  .luhn();
schema.validate(4691050015091614); // Fails
schema.validate(4691050015091615); // Passes
```

## NumberSchema.max

```typescript
max(ceil: number, fail?: ErrorItem): NumberSchema
```

Validates only if the value is smaller than or equal (`<=`) to `ceil`.

```typescript
const schema = validator()
  .number()
  .max(1);
schema.validate(0); // Passes
schema.validate(1); // Passes
schema.validate(2); // Fails
```

## NumberSchema.min

```typescript
min(floor: number, fail?: ErrorItem): NumberSchema
```

Validates only if the value is greater than or equal (`>=`) to `floor`.

```typescript
const schema = validator()
  .number()
  .min(1);
schema.validate(0); // Fails
schema.validate(1); // Passes
schema.validate(2); // Passes
```

## NumberSchema.multipleOf

```typescript
multipleOf(num: number, fail?: ErrorItem): NumberSchema
```

Validates only if the value is a multiple of `num`.

```typescript
const schema = validator()
  .number()
  .multipleOf(3);
schema.validate(-3); // Passes
schema.validate(0); // Passes
schema.validate(1); // Fails
schema.validate(3); // Passes
schema.validate(6); // Passes
```

## NumberSchema.nonnegative

```typescript
nonnegative(fail?: ErrorItem): NumberSchema
```

Validates only if the value is non-negative, i.e. greater than or equal to zero.

```typescript
const schema = validator()
  .number()
  .nonnegative();
schema.validate(-1); // Fails
schema.validate(0); // Passes
schema.validate(1); // Passes
```

## NumberSchema.positive

```typescript
positive(fail?: ErrorItem): NumberSchema
```

Validates only if the value is positive, i.e. greater than to zero.

```typescript
const schema = validator()
  .number()
  .positive();
schema.validate(-1); // Fails
schema.validate(0); // Fails
schema.validate(1); // Passes
```

## NumberSchema.prime

```typescript
prime(fail?: ErrorItem): NumberSchema
```

Validates only if the value is a prime number. It is using a proven algorithm (trial division with 6k+1 optimization), so it is slow. Use a proper tool to evaluate primality for large numbers or for numbers in large quantity.

```typescript
const schema = validator()
  .number()
  .prime();
schema.validate(-2); // Fails
schema.validate(0); // Fails
schema.validate(1); // Fails
schema.validate(2); // Passes
schema.validate(3); // Passes
```

## NumberSchema.range

```typescript
range(
  bound: [number | undefined, number | undefined],
  fail?: ErrorItem
): NumberSchema
```

Validates only if the value is within the bounds (open interval). The `bound` array takes two `number | undefined` values. The first is the lower bound and the second element is the upper bound. The `undefined` value means no bound.

```typescript
const schema1 = validator()
  .number()
  .range([0, 1]);
schema1.validate(0); // Fails
schema1.validate(0.5); // Passes
schema1.validate(1); // Fails

const schema2 = validator()
  .number()
  .range([undefined, 1]);
schema2.validate(0); // Passes
schema2.validate(0.5); // Passes
schema2.validate(1); // Fails
```

## NumberSchema.regex

```typescript
regex(pattern: RegExp, fail?: ErrorItem): NumberSchema
regexp(pattern: RegExp, fail?: ErrorItem): NumberSchema
```

Validates only if the value matches the `pattern`. As RegExp can be run against a string, the value is first converted to a string.

```typescript
const schema = validator()
  .number()
  .regex(/^\d{4}$/);
schema.validate(0); // Fails
schema.validate(123); // Fails
schema.validate(1234); // Passes
```

## NumberSchema.and

```typescript
and(
  options: Array<(sch: NumberSchema) => NumberSchema>,
  fail?: ErrorItem
): NumberSchema
```

Validates only if every NumberSchema of `options` validates. This operator is generally not a useful one, because the schema chain is evaluated with an AND logic.

```typescript
const schema1 = validator()
  .number()
  .and([sch => sch.min(0), sch => sch.max(1)]);
const schema2 = validator()
  .number()
  .min(0)
  .max(1);

schema1.validate(0).valid === schema2.validate(0).valid; // Passes
schema1.validate(2).valid === schema2.validate(2).valid; // Fails
```

## NumberSchema.or

```typescript
or(
  options: options: Array<(sch: NumberSchema) => NumberSchema>,
  fail?: ErrorItem
): NumberSchema
```

Validates only if some NumberSchema of `options` validates.

```typescript
const schema = validator()
  .number()
  .or([sch => sch.min(0).max(10), sch => sch.min(1000).max(9999)]);
schema.validate(5); // Passes
schema.validate(55); // Fails
schema.validate(555); // Fails
schema.validate(5555); // Passes
```

## NumberSchema.not

```typescript
not(condition: (sch: NumberSchema) => NumberSchema, fail?: ErrorItem): NumberSchema
```

Validates only if the NumberSchema of `condition` does not validate.

```typescript
const schema = validator()
  .number()
  .not(
    sch => sch.positive()
  );

schema.validate(-1);  // Passes
schema.validate(0);   // Passes
schema.validate(-);   // Fails
```

## NumberSchema.optional

```typescript
optional(): NumberSchema
```

Validates even if the NumberSchema does not validate. The `valid` field will always be true. The `errors` field will still hold the errors. The `passed` field will only contain the validated value if it really did validate.

`optional()` is mainly to be used with Object validation to allow optional fields.

```typescript
const schema = validator()
  .number()
  .optional()
  .equals(0);
schema.validate(0); // Passes
/*
  result = {
    valid: true,
    errors: undefined,
    passed: 0
  }
*/
schema.validate(1); // Passes
/*
  result = {
    valid: true,
    errors: {
      "": ["<some-error-message>"]
    },
    passed: undefined
  }
*/
```

# StringSchema

A StringSchema by itself validates that a value is of type `string`. It accepts everything with a `typeof` value `"string"`.

Apart from the basic typechecking the StringSchema provides some utilities to check other properties of the string value.

- [cast](#stringschemacast): StringSchema;
- [binary](#stringschemabinary)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [creditcard](#stringschemacreditcard)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [decimal](#stringschemadecimal)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [equals](#stringschemaequals)(str: string, fail?: [ErrorItem](#erroritem)): StringSchema;
- [equalsOne](#stringschemaequalsone)(matches: string[], fail?: [ErrorItem](#erroritem)): StringSchema;
- [hasLowercase](#stringschemahaslowercase)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [hasUppercase](#stringschemahasuppercase)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [hasNumber](#stringschemahasnumber)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [hasPunctuation](#stringschemahaspunctuation)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [hexa](#stringschemahexa)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [luhn](#stringschemaluhn)(fail?: [ErrorItem](#erroritem)): StringSchema;
- [minLength](#stringschemaminlength)(len: number, fail?: [ErrorItem](#erroritem)): StringSchema;
- [maxLength](#stringschemamaxlength)(len: number, fail?: [ErrorItem](#erroritem)): StringSchema;
- [regex](#stringschemaregex)(pattern: RegExp, fail?: [ErrorItem](#erroritem)): StringSchema;
- [regexp](#stringschemaregex)(pattern: RegExp, fail?: [ErrorItem](#erroritem)): StringSchema;
- [and](#stringschemaand)(options: Array<(sch: StringSchema) => StringSchema>, fail?: [ErrorItem](#erroritem)): StringSchema;
- [or](#stringschemaor)(options: Array<(sch: StringSchema) => StringSchema>, fail?: [ErrorItem](#erroritem)): StringSchema;
- [not](#stringschemanot)(condition: (sch: StringSchema) => StringSchema, fail?: [ErrorItem](#erroritem)): StringSchema;
- validate(value: number): [ISimpleResult](#isimpleresult)

## StringSchema.cast

```typescript
cast(): StringSchema
```

If the value is not a string, the validator attempts to run `toString()` first. The converted value is accessible through the [validation result object's](#isimpleresult) `passed` field.

```typescript
const withCast = validator()
  .string()
  .cast();
const noCast = validator().string();

withCast.validate(0); // Passes
noCast.validate(0); // Fails
```

## StringSchema.binary

```typescript
binary(fail?: ErrorItem): StringSchema
```

It validates if the value contains a binary value, that is `/^[01]+$/` allowing leading zeros.

```typescript
const schema = validator()
  .string()
  .binary();

schema.validate(""); // Fails
schema.validate("0"); // Passes
schema.validate("1"); // Passes
schema.validate("2"); // Fails
```

## StringSchema.creditcard

```typescript
creditcard(fail?: ErrorItem): StringSchema
```

It validates if the value conforms to a creditcard sequence, i.e. is matches `/^\d{4}[\s\-]{0,1}\d{4}[\s\-]{0,1}\d{4}[\s\-]{0,1}\d{4}$/` (four digits separated by nothing or space or a dash) and it is a [Luhn formula](#stringschemaluhn).

```typescript
const schema = validator()
  .string()
  .creditcard();

schema.validate("4691050015091615"); // Passes
schema.validate("4691 0500 1509 1615"); // Passes
schema.validate("4691-0500-1509-1615"); // Passes
schema.validate("4691-0500-1509-1614"); // Fails
```

## StringSchema.decimal

```typescript
decimal(fail?: ErrorItem): StringSchema
```

It validates if the value contains a decimal value, that is `/^[0-9]+$/` (allowing leading zeros).

```typescript
const schema = validator()
  .string()
  .decimal();

schema.validate(""); // Fails
schema.validate("0"); // Passes
schema.validate("1"); // Passes
schema.validate("9"); // Passes
schema.validate("A"); // Fails
```

## StringSchema.equals

```typescript
equals(match: string, fail?: ErrorItem): StringSchema
```

It validates if the value equals to (`===`) the `match` string.

```typescript
const schema = validator()
  .string()
  .equals("aBc");

schema.validate("abc"); // Fails
schema.validate("aBc"); // Passes
```

## StringSchema.equalsOne

```typescript
equalsOne(matches: string[], fail?: ErrorItem): StringSchema
```

It validates if the value equals to (`===`) one of the elements of the `matches` argument.

```typescript
const schema = validator()
  .string()
  .equalsOne(["a", "c"]);

schema.validate("a"); // Passes
schema.validate("b"); // Fails
schema.validate("c"); // Passes
```

## StringSchema.hasLowercase

```typescript
hasLowercase(fail?: ErrorItem): StringSchema
```

It validates if the value contains lowercase characters. Since Javascript engines generally do not support the `/\p{Ll}/` regex, the validator uses a different approach where it convers the value `toUppercase()` and checks if there are differences.

```typescript
const schema = validator()
  .string()
  .hasLowercase();

schema.validate("a"); // Passes
schema.validate("A"); // Fails
```

## StringSchema.hasUppercase

```typescript
hasUppercase(fail?: ErrorItem): StringSchema
```

It validates if the value contains uppercase characters. Since Javascript engines generally do not support the `/\p{Lu}/` regex, the validator uses a different approach where it convers the value `toLowercase()` and checks if there are differences.

```typescript
const schema = validator()
  .string()
  .hasUppercase();

schema.validate("a"); // Fails
schema.validate("A"); // Passes
```

## StringSchema.hasNumber

```typescript
hasNumber(fail?: ErrorItem): StringSchema
```

It validates if the value contains numeric characters. Since Javascript engines generally do not support the `/\p{Nd}/` regex, the validator uses the `/\d/`.

```typescript
const schema = validator()
  .string()
  .hasNumber();

schema.validate("elite"); // Fails
schema.validate("l33t"); // Passes
```

## StringSchema.hasPunctuation

```typescript
hasPunctuation(fail?: ErrorItem): StringSchema
```

It validates if the value contains certain punctuations. Since Javascript engines generally do not support the `/\p{P}/` regex, the validator uses the `/[\.\,\?\!\;]/`.

```typescript
const schema = validator()
  .string()
  .hasPunctuation();

schema.validate("Do"); // Fails
schema.validate("Do!"); // Passes
```

## StringSchema.hexa

```typescript
hexa(fail?: ErrorItem): StringSchema
```

It validates if the value contains a hexadecimal value, that is `/^[0-9a-fA-F]+$/` (allowing leading zeros).

```typescript
const schema = validator()
  .string()
  .hexa();

schema.validate(""); // Fails
schema.validate("0"); // Passes
schema.validate("1"); // Passes
schema.validate("9"); // Passes
schema.validate("A"); // Passes
schema.validate("F"); // Passes
schema.validate("G"); // Fails
```

## StringSchema.luhn

```typescript
luhn(fail?: ErrorItem): StringSchema
```

Validates only if the number sequence of the value conforms to the [Luhn formula](https://en.wikipedia.org/wiki/Luhn_algorithm). This formula can be used to check the syntactic validity of credit card or IMEI numbers. Before validation, the value is stripped of every character other than digits.

```typescript
const schema = validator()
  .string()
  .luhn();
schema.validate("79927398713"); // Passes
schema.validate("79927398714"); // Fails
schema.validate("4691x0500x1509x1615"); // Passes
schema.validate("4691-0500-1509-1615"); // Passes
```

## StringSchema.minLength

```typescript
minLength(len: number, fail?: ErrorItem): StringSchema
```

Validates only if the value is longer than or equal to `len`.

```typescript
const schema = validator()
  .string()
  .minLength(1);
schema.validate(""); // Fails
schema.validate("a"); // Passes
schema.validate("aa"); // Passes
```

## StringSchema.maxLength

```typescript
maxLength(len: number, fail?: ErrorItem): StringSchema
```

Validates only if the value is shorter than or equal to `len`.

```typescript
const schema = validator()
  .string()
  .maxLength(1);
schema.validate(""); // Passes
schema.validate("a"); // Passes
schema.validate("aa"); // Fails
```

## StringSchema.regex

```typescript
regex(pattern: RegExp, fail?: ErrorItem): StringSchema
regexp(pattern: RegExp, fail?: ErrorItem): StringSchema
```

Validates only if the value matches the `pattern`.

```typescript
const schema = validator()
  .string()
  .regex(/^\d{4}$/);
schema.validate(0); // Fails
schema.validate(123); // Fails
schema.validate(1234); // Passes
```

## StringSchema.and

```typescript
and(
  options: Array<(sch: StringSchema) => StringSchema>,
  fail?: ErrorItem
): StringSchema
```

Validates only if every StringSchema of `options` validates. This operator is generally not a useful one, because the schema chain is evaluated with an AND logic.

```typescript
const schema1 = validator()
  .string()
  .and([sch => sch.minLength(0), sch => sch.maxLength(1)]);
const schema2 = validator()
  .string()
  .minLength(0)
  .maxLength(1);

schema1.validate("a").valid === schema2.validate("a").valid; // Passes
schema1.validate("aa").valid === schema2.validate("aa").valid; // Fails
```

## StringSchema.or

```typescript
or(
  options: options: Array<(sch: StringSchema) => StringSchema>,
  fail?: ErrorItem
): StringSchema
```

Validates only if some StringSchema of `options` validates.

```typescript
const schema = validator()
  .string()
  .or([sch => sch.minLength(1).maxLength(2), sch => sch.minLength(4).maxLength(4)]);
schema.validate("84"); // Passes
schema.validate("984"); // Fails
schema.validate("1984"); // Passes
```

## StringSchema.not

```typescript
not(condition: (sch: StringSchema) => StringSchema, fail?: ErrorItem): StringSchema
```

Validates only if the StringSchema of `condition` does not validate.

```typescript
const schema = validator()
  .string()
  .not(sch => sch.hasNumber());

schema.validate("elite"); // Passes
schema.validate("l33t"); // Fails
```

## StringSchema.optional

```typescript
optional(): StringSchema
```

Validates even if the StringSchema does not validate. The `valid` field will always be true. The `errors` field will still hold the errors. The `passed` field will only contain the validated value if it really did validate.

`optional()` is mainly to be used with Object validation to allow optional fields.

```typescript
const schema = validator()
  .string()
  .optional()
  .equals("ok");
schema.validate("ok"); // Passes
/*
  result = {
    valid: true,
    errors: undefined,
    passed: "ok"
  }
*/
schema.validate("not ok"); // Passes
/*
  result = {
    valid: true,
    errors: {
      "": ["<some-error-message>"]
    },
    passed: undefined
  }
*/
```

# BooleanSchema

A BooleanSchema by itself validates that a value is of type `boolean`. It accepts everything with a `typeof` value `"boolean"`.

Apart from the basic typechecking the BooleanSchema provides some utilities to check other properties of the string value.

- [cast](#booleanschemacast): BooleanSchema;
- [true](#booleanschemactrue)(fail?: [ErrorItem](#erroritem)): BooleanSchema;
- [false](#booleanschemafalse)(fail?: [ErrorItem](#erroritem)): BooleanSchema;
- [and](#booleanschemaand)(options: Array<(sch: BooleanSchema) => BooleanSchema>, fail?: [ErrorItem](#erroritem)): BooleanSchema;
- [or](#booleanschemaor)(options: Array<(sch: BooleanSchema) => BooleanSchema>, fail?: [ErrorItem](#erroritem)): BooleanSchema;
- [not](#booleanschemanot)(condition: (sch: BooleanSchema) => BooleanSchema, fail?: [ErrorItem](#erroritem)): BooleanSchema;
- validate(value: boolean): [ISimpleResult](#isimpleresult)

## BooleanSchema.cast

```typescript
cast(): BooleanSchema
```

If the value is not a boolean, the validator attempts to convert it first using the double bang method `!!value`. The converted value is accessible through the [validation result object's](#isimpleresult) `passed` field.

```typescript
const withCast = validator()
  .boolean()
  .cast();
const noCast = validator().boolean();

withCast.validate("a"); // Passes
noCast.validate("a"); // Fails
```

## BooleanSchema.true

```typescript
true(): BooleanSchema
```

It validates only if the value is strictly true. When used with [cast](#booleanschemacast), it validates when the value is truthy.

```typescript
const schema = validator()
  .boolean()
  .true();
schema.validate("ok"); // Fails
schema.validate(false); // Fails
schema.validate(true); // Passes
```

## BooleanSchema.false

```typescript
false(): BooleanSchema
```

It validates only if the value is strictly false. When used with [cast](#booleanschemacast), it validates when the value is falsy.

```typescript
const schema = validator()
  .boolean()
  .false();
schema.validate("ok"); // Fails
schema.validate(false); // Passes
schema.validate(true); // Fails
```

## BooleanSchema.and

```typescript
and(
  options: Array<(sch: BooleanSchema) => BooleanSchema>,
  fail?: ErrorItem
): BooleanSchema
```

Validates only if every BooleanSchema of `options` validates. This operator is generally not a useful one, because the schema chain is evaluated with an AND logic. Also, with only `false()` and `true()` validations, it is hard to connect them into an AND chain in a useful manner.

## BooleanSchema.or

```typescript
or(
  options: Array<(sch: BooleanSchema) => BooleanSchema>,
  fail?: ErrorItem
): BooleanSchema
```

Validates only if any BooleanSchema of `options` validates. This operator is not very useful for booleans, as `true()` OR `false()` validation can be achieved by using only `validator.boolean()`.

## BooleanSchema.not

```typescript
not(condition: (sch: BooleanSchema) => BooleanSchema, fail?: ErrorItem): BooleanSchema
```

Validates only if the BooleanSchema of `condition` does not validate. This operator is not very useful for booleans, as `false()` is easier to use than "NOT TRUE".

## BooleanSchema.optional

```typescript
optional(): BooleanSchema
```

Validates even if the BooleanSchema does not validate. The `valid` field will always be true. The `errors` field will still hold the errors. The `passed` field will only contain the validated value if it really did validate.

`optional()` is mainly to be used with Object validation to allow optional fields.

```typescript
const schema = validator()
  .boolean()
  .optional()
  .true();
schema.validate(true); // Passes
schema.validate(false); // Passes
```

# ArraySchema

An ArraySchema by itself validates that a value is an Array. It accepts everything that passes the `Array.isArray` test.

Apart from the typechecking the ArraySchema provides some utilities to check other properties of the array and its elements.

- [each](#arrayschemaeach)(validation: [IValidateable](#ivalidateable) | [ValidatorFn](#validatorfn), fail?: [ErrorItem](#erroritem)): ArraySchema
- [maxLength](#arrayschemamaxlength)(limit: number, fail?: [ErrorItem](#erroritem)): ArraySchema;
- [minLength](#arrayschemaminlength)(limit: number, fail?: [ErrorItem](#erroritem)): ArraySchema;
- [length](#arrayschemalength)(length: number, fail?: [ErrorItem](#erroritem)): ArraySchema
- [value](#arrayschemavalue)(index: number, schema: [IValidateable](#ivalidateable), fail?: [ErrorItem](#erroritem)): ArraySchema
- [values](#arrayschemavalues)(schemas: [IValidateable[]](#ivalidateable), fail?: [ErrorItem](#erroritem)): ArraySchema
- validate(value: any[]): [IComplexResult](#icomplexresult)

## ArraySchema.each

```typescript
each(
  validation: IValidateable | ValidatorFn,
  fail?: ErrorItem,
): ArraySchema
```

It validates only if every element in the array passes the `validation`.

A [ValidatorFn](#validatorfn) is just a function that gets called for each element with the arguments of the current element, the index and the entire value, and it returns a [result object](#iresult), i.e. it creates a validator and returns the result of its `validate` method. This provides a means to create more flexible validations that can take into account the elements position or the properties of the whole array. See the example of [Validate array elements in a pattern](./COOKBOOK.md#validatearrayelementsinapattern).

```typescript
const schema1 = validator()
  .array()
  .each(
    validator()
      .string()
      .minLength(1)
  );

schema1.validate(["a", ""]); // Fails
schema1.validate(["a", "b"]); // Passes

const schema2 = validator()
  .array()
  .each((elem, index, arr) =>
    validator()
      .string()
      .minLength(index)
      .validate(elem)
  );
// Each element must be a string,
// and the given element's length
// must correlate with its position.

schema2.validate(["", "b", "c"]); // Fails
schema2.validate(["", "b", "cc"]); // Passes
```

## ArraySchema.maxLength

```typescript
maxLength(limit: number, fail?: ErrorItem): ArraySchema
```

Validates only if the array is shorter than or equal to `limit`.

```typescript
const schema = validator()
  .array()
  .maxLength(1);
schema.validate([]); // Passes
schema.validate(["a"]); // Passes
schema.validate(["a", "a"]); // Fails
```

## ArraySchema.minLength

```typescript
minLength(limit: number, fail?: ErrorItem): ArraySchema
```

Validates only if the array is longer than or equal to `limit`.

```typescript
const schema = validator()
  .array()
  .minLength(1);
schema.validate([]); // Fails
schema.validate(["a"]); // Passes
schema.validate(["a", "a"]); // Passes
```

## ArraySchema.length

```typescript
length(length: number, fail?: ErrorItem): ArraySchema
```

Validates only if the array's length equal to `length`.

```typescript
const schema = validator()
  .array()
  .length(1);
schema.validate([]); // Fails
schema.validate(["a"]); // Passes
schema.validate(["a", "a"]); // Fails
```

## ArraySchema.value

```typescript
value(index: number, schema: IValidateable, fail?: ErrorItem): ArraySchema
```

Validates only if the element at the `index` position validates for the `schema`.

```typescript
const schema = validator()
  .array()
  .value(1, validator().number());
schema.validate(["a", "a", "a"]); // Fails
schema.validate(["a", 1, "a"]); // Passes
```

## ArraySchema.values

```typescript
values(schemas: IValidateable[], fail?: ErrorItem): ArraySchema
```

Validates only if elements validate for the `schemas` respectively, i.e. `value[0]` validates for `schemas[0]` and so on. If there are more elements in the value than schemas defined then the rest are left unvalidated. If you want to limit the validated value to a fixed number use [length()](#arrayschemaslength)!

```typescript
const schema = validator()
  .array()
  .values([validator().number(), validator().string()]);
schema.validate(["a", 1]); // Fails
schema.validate([1, "a"]); // Passes
schema.validate([1, "a", 2]); // Passes
```

## ArraySchema.optional

```typescript
optional(): ArraySchema
```

Validates even if the ArraySchema does not validate. The `valid` field will always be true. The `errors` field will still hold the errors. The `passed` field will only contain the validated value if it really did validate.

`optional()` is mainly to be used with Object validation to allow optional fields.

```typescript
const schema = validator()
  .array()
  .optional()
  .minLength(1);
schema.validate(["ok"]); // Passes
/*
  result = {
    valid: true,
    errors: undefined,
    passed: ["ok"]
  }
*/
schema.validate([]); // Passes
/*
  result = {
    valid: true,
    errors: {
      "": ["<some-error-message>"]
    },
    passed: undefined
  }
*/
```

# JSONSchema

A JSONSchema by itself validates that a value is an Object. It accepts everything with a `typeof` value of `"object"` and that has a method called `hasOwnProperty`. The latter makes it exclude special objects like `Object.create(null)`. Since it is a JSONSchema if the value passed to it is of type string, it first attempts to parse it with `JSON.parse`. The parsed value is accessible through the [validation result object's](#icomplexresult) `passed` field.

Apart from the typechecking the JSONSchema provides some utilities to check other properties of the object and the properties of its keys.

- [allowedKeys](#jsonschemaallowedkeys)(keys: string[], fail?: [ErrorItem](#erroritem)): JSONSchema
- [keys](#jsonschemakeys)(keys: { [key: string]: [IValidateable](#ivalidateable) }, fail?: [ErrorItem](#erroritem)): JSONSchema;
- validate(value: any[]): [IComplexResult](#icomplexresult)

## JSONSchema.allowedKeys

```typescript
allowedKeys(keys: string[], fail?: ErrorItem): JSONSchema
```

It validates only if every field (returned by `Object.keys`) is included in the `keys`.

```typescript
const schema = validator()
  .json()
  .allowedKeys(["bar"]);

schema.validate({ foo: "bar" }); // Fails
schema.validate({ bar: "foo" }); // Passes
```

## JSONSchema.each

```typescript
each(
  validation: IValidateable,
  fail?: ErrorItem,
): JSONSchema
```

It validates only if every property/key (gathered with Object.keys) in the object passes the `validation`. This is useful when validating mapped types.

```typescript
const schema = validator()
  .json()
  .each(
    validator()
      .string()
      .minLength(3)
  );

schema.validate({ foo: "bar", bar: "foo" }); // Passes
schema.validate({ bar: "foo", bar: 3 }); // Fails
```

## JSONSchema.keys

```typescript
keys(keys: { [key: string]: IValidateable }, fail?: ErrorItem): JSONSchema
```

It validates only if every keys validates that has a validation defined for it. Keys without a defined validation are left unvalidated. If you want the object to only contain keys that are validated, use [allowedKeys](#jsonschemaallowedkeys).

```typescript
const schema = validator()
  .json()
  .keys({
    foo: validator()
      .array()
      .each(validator().string()),
  });

schema.validate({
  // Fails
  foo: "bar",
});
schema.validate({
  // Passes
  foo: ["bar"],
});
schema.validate({
  // Passes
  foo: ["bar"],
  boo: "bah",
});
```

## JSONSchema.optional

```typescript
optional(): JSONSchema
```

Validates even if the JSONSchema does not validate. The `valid` field will always be true. The `errors` field will still hold the errors. The `passed` field will only contain the validated value if it really did validate.

`optional()` is mainly to be used with Object validation to allow optional fields.

```typescript
const schema = validator()
  .json()
  .optional()
  .allowedKeys(["foo"]);
schema.validate({ foo: "ok" }); // Passes
/*
  result = {
    valid: true,
    errors: undefined,
    passed: { 'foo': 'ok' }
  }
*/
schema.validate({ foo: "ok", bar: "not okay" }); // Passes
/*
  result = {
    valid: true,
    errors: {
      "": ["<some-error-message>"]
    },
    passed: undefined
  }
*/
```

# Misc

## Types

### ErrorItem

ErrorItem is a type alias for the `any` type. Arguments of the type of ErrorItem can be used to provide custom error messages for failed validations.

```typescript
validator().number("Not a number!");
validator().array(1001);
validator().string({ code: 1002, message: "Not a string!" });
```

### ErrorList

ErrorList is a type alias for the `ErrorItem[]`.

### ValidatorFn

ValidatorFn is a type alias for `(elem: any, index: number, arr: any[]) => IResult` used by [ArraySchema's each](#arrayschemaeach) method. It can be used to create a context-dependent validation for the elements of the array.

## Interfaces

### IErrorDict

```typescript
interface IErrorDict {
  "": ErrorList;
}
```

The typical error construct where the `""` field contains the errors pertaining to the validated element itself. As opposed to `{ "foo": ["bar"]}` where the error is related to the `foo` field of the validated element, as seen in [IErrorJSON](#ierrorjson).

### IErrorJSON

```typescript
interface IErrorJSON {
  [key: string]: ErrorList | IErrorJSON;
}
```

The error construct for complex types like `Array` and `Object`/`JSON`, where the `""` field contains the errors pertaining to the validated element itself (like the array or object as a whole), and the other keys are related to the respective fields of the validated element. See [IComplexResult](#icomplexresult) for an example.

### IValidateable

Objects that implement this interface provide access to a `validate(value: any)` function.

### ILockedAnySchema

A dummy interface for [AnySchema](#AnySchema) which gives access only to the `validate()` function on AnySchema. As such, it is not much different than the interface [IValidateable](#ivalidateable).

ILockedAnySchema is returned after a logical function has been called on AnySchema, e.g. `and()`, `or()` or `not()`. Because one AnySchema can only validate in one of these modes, chain calling them would make no sense.

```typescript
validator()
  .or([
    // This call is registered!
    validator().string(),
    validator().number(),
  ])
  .not(
    // This method is not available in code completion tools due to ILockedAnySchema.
    validator().array() // "Force-calling" it has no effect, the schema in 'not' will not be validated.
  );
```

### IResult

```typescript
interface IResult {
  valid: boolean;
  errors: { [key: string]: any } | undefined;
}
```

This is an interface for the result objects that are returned by the `validate()` functions.

### ILogicResult

```typescript
interface ILogicResult extends IResult {
  valid: boolean;
  errors: (IErrorJSON & IErrorDict) | undefined;
  results: Array<IResult | ILogicResult>;
}
```

This is an interface for the result object of the [AnySchema](#anyschema). The `results` field contains the results of the validations that are defined in [and](#anyschemaand), [or](#anyschemaor) or [not](#anyschemanot).

### ISimpleResult

```typescript
interface ISimpleResult<T> extends IResult {
  valid: boolean;
  errors: IErrorDict | undefined;
  passed: T | undefined;
}
```

This is an interface for the result object of the SimpleSchemas like [StringSchema](#stringschema) and [NumberSchema](#numberschema).

The `valid` indicates the result of the validation.

The `errors` contains error messages regarding the validated element. See: [IErrorDict](#ierrordict))

The `passed` field contains the value if the validation was successful. This is useful when the value has been transformed or `cast()` because the transformed value is accessible through this field.

```typescript
const withCast = validator()
  .number("Not a number!")
  .cast();
const noCast = validator().number("Not a number!");
const testValue: any = "0";

const castResult = withCast.validate(testValue);
expect(castResult.valid).toBe(true);
expect(castResult.errors).toEqual(undefined);
expect(castResult.passed).toEqual(0);

const noCastResult = noCast.validate(testValue);
expect(noCastResult.valid).toBe(false);
expect(noCastResult.errors).toEqual({ "": ["Not a number!"] });
expect(noCastResult.passed).toEqual(undefined);
```

### IComplexResult

```typescript
interface IComplexResult<T> extends IResult {
  valid: boolean;
  errors: IErrorJSON | IErrorDict | undefined;
  passed: T | undefined;
}
```

This is an interface for the result object of the ComplexSchemas like [ArraySchema](#arrayschema) and [JSONSchema](#jsonschema).

The `valid` indicates the result of the validation.

The `errors` contains error messages regarding the validated element. See: [IErrorDict](#ierrordict)) and [IErrorJSON](#ierrorjson) - depending on the given field/element being validated.

The `passed` field contains the values where validation was successful.

```typescript
const schema = validator()
  .json("Not a JSON!")
  .keys(
    {
      foo: validator()
        .string("Not a string!")
        .minLength(4, "Min 4 characters!")
        .decimal("Only numbers!"),
    },
    "Key check failed!"
  );

let result;
result = schema.validate(0 as any);
expect(result.valid).toBe(false);
expect(result.errors).toEqual({ "": ["Not a JSON!"] });
expect(result.passed).toEqual(undefined);

result = schema.validate({} as any);
expect(result.valid).toBe(false);
expect(result.errors).toEqual({
  "": ["Key check failed!"],
  foo: {
    "": ["Not a string!"],
  },
});
expect(result.passed).toEqual({});

result = schema.validate({ foo: "bar" } as any);
expect(result.valid).toBe(false);
expect(result.errors).toEqual({
  "": ["Key check failed!"],
  foo: {
    "": ["Min 4 characters!", "Only numbers!"],
  },
});
expect(result.passed).toEqual({});

result = schema.validate({ foo: "bara" } as any);
expect(result.valid).toBe(false);
expect(result.errors).toEqual({
  "": ["Key check failed!"],
  foo: {
    "": ["Only numbers!"],
  },
});
expect(result.passed).toEqual({});

result = schema.validate({ foo: "123" } as any);
expect(result.valid).toBe(false);
expect(result.errors).toEqual({
  "": ["Key check failed!"],
  foo: {
    "": ["Min 4 characters!"],
  },
});
expect(result.passed).toEqual({});

result = schema.validate({ foo: "1234" } as any);
expect(result.valid).toBe(true);
expect(result.errors).toEqual(undefined);
expect(result.passed).toEqual({ foo: "1234" });
```
