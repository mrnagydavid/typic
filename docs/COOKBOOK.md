# Add custom error messages with codes and whatnot

```typescript
const schema = validator()
  .string()
  .minLength(8, { code: 1000, message: "Password should be at least 8 characters long!" })
  .maxLength(64, { code: 1001, message: "Password should not be more than 64 characters long!" })
  .hasNumber({ code: 1002, message: "Password should contain numbers!" })
  .hasUppercase({ code: 1003, message: "Password should contain uppercase letters!" });

const { valid, errors } = schema.validate("x");
/*
  errors = {
    "": [
      { code: 1000, message: "Password should be at least 8 characters long!" },
      { code: 1002, message: "Password should contain numbers!" },
      { code: 1003, message: "Password should contain uppercase letters!" },
    ]
  }
*/
```

# Validate union arguments

```typescript
function foo(bar: string | number) {
  const barSchema = validator().or([validator().string(), validator().number()]);
}
```

# Validate TypeScript Enum

```typescript
enum Yoda {
  DO,
  DO_NOT,
}

const schema = validator()
  .string()
  .equalsOne(Object.keys(Yoda))
  .not(sch => sch.decimal());
```

# Validate each array element

For a `string[]`:

```typescript
const arraySchema = validator()
  .array()
  .each(validator().string());
```

For a `Array<string | number>`:

```typescript
const arraySchema = validator()
  .array()
  .each(validator().or([validator().string(), validator().number()]));
```

# Validate array elements in a pattern

For an array with `string` + `number` + `number` repeating over and over...

```typescript
const schemaPattern = [validator().string(), validator().number(), validator().number()];

const validatorFn = (elem: any, index: number, arr: any[]) =>
  schemaPattern[index % schemaPattern.length].validate(elem);

const arraySchema = validator()
  .array()
  .each(validatorFn);

const jsonSchema = validator()
  .json()
  .keys({
    length: validator()
      .number()
      .min(3)
      .multipleOf(3),
  });

const schema = validator().and([jsonSchema, arraySchema]);

// schema.validate(...)
```

# Filter invalid optional fields (e.g. during registration)

```typescript
const allowedFields = ["email", "password", "name", "age", "location"];
const email = validator()
  .string()
  .regex(/\@/);
const password = validator()
  .string()
  .minLength(8)
  .maxLength(64)
  .hasLowercase()
  .hasUppercase()
  .hasNumber();
const name = validator().string();
const age = validator()
  .number()
  .min(13)
  .max(99);
const location = validator().string();
const fullSchema = validator()
  .json()
  .allowedKeys(allowedFields)
  .keys({
    email,
    password,
    name,
    age,
    location,
  });
const requiredSchema = validator()
  .json()
  .keys({
    email,
    password,
  });

const requestObject = {
  /* incoming */
};
const { passed } = fullSchema.validate(requestObject);
const result = requiredSchema.validate(passed);

if (result.valid) {
  /* 
    We have the minimum required fields to continue the process
    The `passed` field might contain additional fields too,
    but it will not contain invalid optional fields,
    nor fields that were not permitted in `allowedFields`
  */
} else {
  /*
    The request didn't satisfy the minimum criteria
    of a successful registration.
  */
}
```

# Validate without invalid optional fields

Similarly to the previous example. You can set certain fields to be optional.

```typescript
const schema = validator()
  .json()
  .keys({
    email: validator()
      .string()
      .regex(/@/),
    name: validator()
      .string()
      .optional() // !!!
      .minLength(5, "Not long enough!"),
  });

const result = schema.validate({
  email: "email.address.com",
  name: "x",
});

/*
  result = {
    valid: true,
    errors: {
      "name": {
        "": ["Not long enough!"]
      }
    },
    passed: {
      email: 'email@address.com',
    }
  }
*/
```

# Validate maps

We want to validate mapped results, where the JSON key can be anything (any string, that is), but the values should conform to a format.

```typescript
const userSchema = validator()
  .json()
  .keys({
    name: validator()
      .string()
      .minLength(1)
      .maxLength(20),
    password: validator()
      .string()
      .minLength(8)
      .hasUppercase()
      .hasNumber(),
  });
const userListSchema = validator()
  .json()
  .each(userSchema);

const result = userListSchema.validate({
  "user1@domain.com": {
    name: "Justin Beaver",
    password: "Password1",
  },
  "user2@domain.com": {
    name: "Gargamel",
    password: "N05murf5",
  },
});
```
